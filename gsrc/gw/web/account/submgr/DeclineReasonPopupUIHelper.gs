package gw.web.account.submgr

uses pcf.JobComplete
uses java.lang.Long

@Export
class DeclineReasonPopupUIHelper {
  // Loads the Submission from the DB into a separate bundle before
  // declining it, to avoid interference from any validation issues on
  // the current page.
  public static function declineSubmission(submission : entity.Submission, policyPeriod : entity.PolicyPeriod, wizard : pcf.api.Wizard) {

    if (submission.RejectReason == null) {
      throw new gw.api.util.DisplayableException(displaykey.Web.DeclinedReasonPopup.EmptyReasonError)
    }

    var sub : Submission
    var branch : PolicyPeriod
    var rsrPolicy : RSRPolicyData_Ext
    // Execute the decline in a separate bundle with the submission reloaded from the DB
    gw.transaction.Transaction.runWithNewBundle( \b -> {
      sub = b.loadByKey( submission.ID ) as Submission
      branch = b.loadByKey( policyPeriod.ID ) as PolicyPeriod
      // Added for RSR Integration Module -- RSR Fields


    if(submission.RSRSubPolicydata_ExtID.RSRInsBranch != null) {
      rsrPolicy = new RSRPolicyData_Ext(b)
      // But, make sure to update the decline reason
      sub.RejectReason = submission.RejectReason
      sub.RejectReasonText = submission.RejectReasonText
      rsrPolicy.rsrCancelReason = submission.RSRSubPolicydata_ExtID.rsrCancelReason
      rsrPolicy.RSRInsBranch  = submission.RSRSubPolicydata_ExtID.RSRInsBranch
      rsrPolicy.RSRSubBranch = submission.RSRSubPolicydata_ExtID.RSRSubBranch
      rsrPolicy.rsrNumber = submission.RSRSubPolicydata_ExtID.rsrNumber
      rsrPolicy.rsrReasonCode = submission.RSRSubPolicydata_ExtID.rsrReasonCode
      rsrPolicy.FSMACode = submission.RSRSubPolicydata_ExtID.FSMACode
      rsrPolicy.disputeRegis = submission.RSRSubPolicydata_ExtID.disputeRegis
      rsrPolicy.doubtableAddr = submission.RSRSubPolicydata_ExtID.doubtableAddr
      rsrPolicy.eventDate = submission.RSRSubPolicydata_ExtID.eventDate
      rsrPolicy.regisDate = submission.RSRSubPolicydata_ExtID.regisDate
      rsrPolicy.contactID = submission.RSRSubPolicydata_ExtID.contactID
      rsrPolicy.comDriverID = submission.RSRSubPolicydata_ExtID.comDriverID
      sub.RSRSubPolicydata_ExtID = rsrPolicy
      branch.addEvent(gw.acc.rsr.RSRConstants.DENIAL_PERIOD)
    }
      // Decline and commit
      branch.SubmissionProcess.declineJob()
    })


    // Go to JobComplete page if we're declining from a wizard
    if (wizard != null) {
      wizard.cancel()
      JobComplete.go(sub, policyPeriod)
    }
  }

}