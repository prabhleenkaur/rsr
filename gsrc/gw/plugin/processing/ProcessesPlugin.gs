package gw.plugin.processing
uses gw.plugin.processing.IProcessesPlugin
uses gw.processes.BatchProcess
uses gw.processes.PolicyRenewalClearCheckDate
uses gw.processes.ApplyPendingAccountDataUpdates
uses gw.processes.SolrDataImportBatchProcess
uses gw.acc.rsr.RSRExportCustomBatch
uses gw.acc.rsr.RSRExportCancelCustomBatch
uses gw.acc.rsr.RSRListFileCustomBatch
@Export
class ProcessesPlugin implements IProcessesPlugin {

  construct() {
  }

  override function createBatchProcess(type : BatchProcessType, arguments : Object[]) : BatchProcess {
    switch(type) {
      case BatchProcessType.TC_POLICYRENEWALCLEARCHECKDATE:
          return new PolicyRenewalClearCheckDate()
      case BatchProcessType.TC_APPLYPENDINGACCOUNTDATAUPDATES:
          return new ApplyPendingAccountDataUpdates()
      case BatchProcessType.TC_SOLRDATAIMPORT:
          return new SolrDataImportBatchProcess()
      case BatchProcessType.TC_RSREXPORT_EXT:
          return new RSRExportCustomBatch()
      case BatchProcessType.TC_RSRCANCEL_EXT:
          return new RSRExportCancelCustomBatch()
      case BatchProcessType.TC_RSRLISTFILE_EXT:
          return new RSRListFileCustomBatch()
        default:
        return null
    }
  }

}
