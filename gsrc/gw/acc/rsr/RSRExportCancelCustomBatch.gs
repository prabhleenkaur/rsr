package gw.acc.rsr

uses gw.processes.BatchProcessBase
uses java.lang.Exception
uses java.text.SimpleDateFormat
uses java.util.Date
uses gw.acc.rsr.rsrdb.RSRDBBase
uses java.util.HashMap
uses gw.acc.rsr.RSRData.RSRField
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses java.sql.Timestamp

/**
 * Created with IntelliJ IDEA.
 * User:
 * Shikha Agarwal
 * Date: 03/06/16
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
class RSRExportCancelCustomBatch extends BatchProcessBase {

  public static final var _log: Logger = RSRConstants._log
  static final var CLASS_NAME = "RSRExportCancelCustomBatch :::"
  public var currentDate : Date = gw.api.util.DateUtil.currentDate()


  /**
   * To construct the Batch Process
   */
  construct() {
    super(BatchProcessType.TC_RSRCANCEL_EXT)
  }


  /**
   * Override Function to check the Initial Conditions
   */
  override function checkInitialConditions(): boolean {
    return true
  }


  /**
   * Do Work - To populate the staging table with policies Cancelled 30 days ago.
   */
  override function doWork() {

    var METHOD_NAME = CLASS_NAME + "doWork"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    OperationsExpected = 0
    var diffDate = 0
    //todo: consider suspending destination to ensure data consistency, support for partially broken operations, re-enable destination

    try {
      if (TerminateRequested) {
        throw new Exception("Terminate Requested!")
      }
      var integrationDB = new RSRDBBase()
      var policyPeriodQuery = gw.api.database.Query.make(PolicyPeriod)
      policyPeriodQuery.compare(PolicyPeriod#CANCELLATIONDATE,NotEquals,null)
      var result = policyPeriodQuery.select()
      var recordsSentDatassurQuery = gw.api.database.Query.make(Records_sent_Datassur_Ext)
      recordsSentDatassurQuery.compare(Records_sent_Datassur_Ext#ReasonCode,Equals,12)
      var recordsDatassur = recordsSentDatassurQuery.select()
      for (aPolicyPeriod in result)
      {
        var flag=0
        var todayDate = new Timestamp(currentDate.Time)
        var cancellationDate = new Timestamp(aPolicyPeriod.CancellationDate.Time)
        diffDate = todayDate.differenceInDays(cancellationDate)
        if((aPolicyPeriod.LatestPeriod.Job.Subtype.DisplayName.matches(RSRConstants.cancellation)) and (diffDate < RSRConstants.diffDate) and (aPolicyPeriod.Cancellation.CancelReasonCode.DisplayName.matches(RSRConstants.nonPayment))  and (aPolicyPeriod.EstimatedPremium != null and aPolicyPeriod.EstimatedPremium.Amount>RSRConstants.rsrAmount))
        {
          for (records_sent_Datassur_Ext in recordsDatassur)
          {
            if(aPolicyPeriod.PolicyNumber == records_sent_Datassur_Ext.PolicyNumber)
            {
              flag = 1
              break
            }
          }
          if(flag ==0) {
            integrationDB.insertToRSRStaging(aPolicyPeriod)
          }
        }
      }
      if(_log. isDebugEnabled()) {
        _log.trace(METHOD_NAME + " ****OperationsExpected****" + OperationsExpected)
      }
    } catch (ex: Exception) {
      incrementOperationsFailed()
      OperationsFailedReasons.add(ex.Message)
      _log.error("Exception occured", ex +":::" +ex.StackTraceAsString)
    } finally {
      incrementOperationsCompleted()
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Support termination before each query export in the iteration
   */
  override function requestTermination(): boolean {
    return true
  }


}