package gw.acc.rsr
/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 3/2/16
 * Time: 4:23 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement RSRContactEnhancement : entity.Contact {

  property set select(val : Boolean)  {
    this.select = val
  }


  property get select() : Boolean {

    return this.select //== null ? true : this.select
  }

  property set unSelect(val: Boolean)  {
    this.unSelect = val
  }


  property get unSelect() : Boolean {
    return this.unSelect// == null ? false : this.unSelect
  }

}
