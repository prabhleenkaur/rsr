package gw.acc.rsr.rsrdb

uses gw.acc.rsr.RSRConstants
uses gw.acc.rsr.RSRListVO
uses java.util.ArrayList
uses java.util.Date
uses java.text.SimpleDateFormat
uses gw.api.database.Query
uses gw.util.GosuStringUtil

/**
 * Created with IntelliJ IDEA.
 * User: Prabhleen.Kaur
 * Date: 6/8/16
 * Time: 2:30 PM
 * Mapper class which maps data from Entity to POJO class
 */
class RSRDataMapper {
  var _log = RSRConstants._log
  static final var CLASS_NAME = "RSRDataMapper :::"

  /*
   Function to get records from RSRList table
   */

  @Returns("List of Invalid contacts present in RSR List")
  public  function getRSRInvalidContacts(): List<RSRListVO> {
    var METHOD_NAME = CLASS_NAME + "getRSRInvalidContacts"

    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var rsrListVo: RSRListVO
    var rsrListInvalidContacts = new ArrayList<RSRListVO>()
    var rsrListContacts = Query.make(RSRList_Ext).select().toList()
    for (contact in rsrListContacts) {
      rsrListVo = new RSRListVO()
      if (contact.PersonCompany == 1) {
        rsrListVo.EntityType = contact.PersonCompany
        rsrListVo.LastName = contact.Name
        if (contact.FirstName != null) {
          rsrListVo.FirstName = contact.FirstName
        }
        else {
          rsrListVo.FirstName = ""
        }
        if (contact.DateOfBirth != null) {
          rsrListVo.DateOfBirth = contact.DateOfBirth
          rsrListVo.DobString = convertToDateFormat(contact.DateOfBirth)
        }
        else {
          rsrListVo.DateOfBirth = "" as Date
          rsrListVo.DobString = ""
        }
        rsrListVo.PrimaryAddressDisplayValue = contact.HouseNumber + "," + contact.Street + "," + contact.City + "," + contact.ZipCode
        rsrListVo.Reason = getReason(contact.CodeReason)
        rsrListVo.EntityTypeName = RSRConstants.INDIVIDUAL
        rsrListVo.EventDate = convertToDateFormat(contact.EventDate)
        rsrListVo.RegistrationDate = convertToDateFormat(contact.EventIdentificationDate)
      }
      else if (contact.PersonCompany == 2){
        rsrListVo.Name = contact.Name
        rsrListVo.EntityType = contact.PersonCompany
        if (contact.VATNumber != null) {
          rsrListVo.VatId = contact.VATNumber
        }
        else {
          rsrListVo.VatId = ""
        }
        rsrListVo.EntityTypeName = RSRConstants.LEGAL_ENTITY
        rsrListVo.Reason = getReason(contact.CodeReason)
        rsrListVo.PrimaryAddressDisplayValue = contact.HouseNumber + "," + contact.Street + "," + contact.City + "," + contact.ZipCode
        rsrListVo.EventDate = convertToDateFormat(contact.EventDate)
        rsrListVo.RegistrationDate = convertToDateFormat(contact.EventIdentificationDate)
      }
      rsrListInvalidContacts.add(rsrListVo)
    }
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return rsrListInvalidContacts
  }

  /*Function to convert date into the required format */

  @Returns(" Date in the yyyyMMdd format")
  @Param("dateString", String)
  private static  function convertStringToDate(dateString: String): Date {
    var formatter = new SimpleDateFormat("yyyyMMdd")
    if (dateString != null) {
      return (formatter.parse(dateString) )
    }
    else
      return null
  }

  @Returns("Reason description of the given reason code")
  @Param("reasonCode", String)
  public function getReason(reasonCode: String): String {
    var reason = Query.make(RSRValueCheck_Ext).compare("rsrCode", Equals, reasonCode).select().first()
    var reasonDescription: String = reason.description
    var shortReason: String = GosuStringUtil.EMPTY

    if (reasonDescription?.contains("in case")) {
      //done to extract shorter reason as part of requirement
      shortReason = reasonDescription?.split("in case").toList().get(1)
    }
    else if (reasonDescription?.contains("because")) {
      shortReason = reasonDescription?.split("because").toList().get(1)
    }
    else {
      shortReason = reasonDescription
    }
    return shortReason
  }

  @Returns("Date in a given format")
  @Param("date", Date)
  private static function convertToDateFormat(date: Date): String {
    var dateFormat: SimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy")
    var retDate = dateFormat.format(date)
    return retDate
  }
}