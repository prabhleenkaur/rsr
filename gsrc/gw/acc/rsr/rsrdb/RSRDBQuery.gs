package gw.acc.rsr.rsrdb

uses java.util.HashMap
uses java.util.Arrays

/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 30/10/15
 * Time: 12:40 PM
 * To change this template use File | Settings | File Templates.
 */
final class RSRDBQuery {

  public static final var RSRQuery : HashMap<String, String> = new HashMap<String, String>() {

      "insertToRSRData" -> "INSERT INTO RSRExportData_Ext.dbo.RSRData(POLICYNUMBER,policypublicid,senderid,regis_code,"+
          "rsrversion,CreateTime,UpdateTime,RSRnumber,ins_branch,FSMACode,eventdate,regis_date,"+
          "reasoncode,isdispute,doubtableaddr,amount,name,policyaddr,langcode,entitytype,firstname,"+
          "DOB,gender,orgtype,vatnumber,PRole,ristLocAddr,contactpublicid,NumClaimsInvolved,NumClaimsFault,RSRDossierNumber) VALUES (?,?,?,?,?,?,?,?,"+
          "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",

      "getRSRPolicyData" -> "SELECT name,policyaddr,langcode,entitytype,firstname,DOB,gender,orgtype,vatnumber,"+
          "ristLocAddr,PRole,ins_branch,regis_code,FSMACode,POLICYNUMBER,senderid,"+
          "eventdate,regis_date,reasoncode,regis_code,isdispute,doubtableaddr,amount,rsrversion,"+
          "RSRnumber,NumClaimsInvolved,NumClaimsFault,policypublicid,contactpublicid,multiConInd,RSRDossierNumber FROM RSRExportData_Ext.dbo.RSRData ",

      "deleteStagingData" ->  "delete FROM RSRExportData_Ext.dbo.RSRData",

      "isContactExist" -> "SELECT @@ROWCOUNT FROM RSRExportData_Ext.dbo.RSRData WHERE contactpublicid = ? ;"

  }


  /**
   * Function to get the SQL Query
   */
  public static function getQuery(qName : String) : String {
    return RSRQuery.get(qName)
  }



}