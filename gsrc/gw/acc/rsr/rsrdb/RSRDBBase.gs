package gw.acc.rsr.rsrdb

uses java.lang.Class
uses java.io.Closeable
uses java.lang.Class
uses java.lang.Integer
uses java.sql.Connection
uses java.sql.DriverManager
uses java.sql.ResultSet
uses java.sql.SQLException
uses java.util.Date
uses gw.api.util.DateUtil
uses gw.acc.rsr.RSRPolicyHelper
uses gw.acc.rsr.RSRData
uses java.util.ArrayList
uses gw.lang.ScriptParameters
uses java.util.HashMap
uses gw.acc.rsr.RSRData.RSRField
uses gw.acc.rsr.RSRConstants
uses java.text.SimpleDateFormat
uses java.sql.PreparedStatement
uses java.lang.StringBuilder
uses java.sql.Timestamp
uses java.lang.Exception
uses gw.api.importing.importing.anonymous.attributes.Typekey_Code
uses util.IntegrationPropertiesUtil
/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 29/10/15
 * Time: 3:14 PM
 * To change this template use File | Settings | File Templates.
 */
class RSRDBBase implements Closeable {

  private var _connection: Connection = null
  private static final var _dbName = "RSRExportData_Ext"
  private final var _dbURL =  IntegrationPropertiesUtil.getProperty("RSRDBURL")
  private static final var _log = RSRConstants._log
  private static final var _username = displaykey.Web.Acc.Rsr.UserName
  private var currentUser = User.util.CurrentUser.Credential.UserName
  private var currentDate = new java.util.Date()
  private var mapper = gw.api.util.TypecodeMapperUtil.getTypecodeMapper()
  private var policyHelper = new RSRPolicyHelper()
  private var CLASS_NAME = "gw.acc.rsr.RSRDBBase ::::"

  private var _currentUserName = User.util.CurrentUser.Credential.UserName
  private var _timeStamp = new java.sql.Timestamp( currentDate.getTime() )

  /**
   * Function to get the Connection Properties
   */
  @Returns ("Connection with Sql Server Database")
  public function getConnection(): Connection {
    if (_connection == null or _connection.Closed) {
      Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver")
      _connection = DriverManager.getConnection(_dbURL) //DriverManager.getConnection(_dbURL)
    }
    return _connection
  }

  /**
   * Override method to close the connection
   */
  override function close() {
    if (_connection != null and !_connection.Closed) {
      _connection.close()
    }
  }

  /**
   * Function to insert an update to the data to the staging table -- Insert query
   * @Object
   * [VP]
   **/
  @Param("rsrListUpdate", RSRListUpdate_Ext)
  public function insertUpdateToRSRStaging( rsrListUpdate : RSRListUpdate_Ext ) {

    var METHOD_NAME = CLASS_NAME + "insertUpdateToRSRStaging"

    if ( _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + RSRConstants._methodIN )
    }

    using ( var con = getConnection(),
        var ps = con.prepareStatement( RSRDBQuery.getQuery( "insertToRSRData" ) ) ) {
      prepareUpdateValues( ps, rsrListUpdate )

      ps.addBatch()
      ps.executeBatch()

      if ( _log.isDebugEnabled() ) {
        _log.debug( METHOD_NAME + "The RSR Entry is added to the Staging Table for admin update ..." )
      }
    }
    if ( _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + RSRConstants._methodOUT )
    }
  }


  /**
   * Function to Insert the Data from Automatic batch to the Staging Table
   */
  @Param("policyPeriod", PolicyPeriod)
  public function insertToRSRStaging(policyPeriod : PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "insertToRSRStaging"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    using (
        var con = getConnection(),
        var ps = con.prepareStatement(RSRDBQuery.getQuery("insertToRSRData"))) {
      insertCancellationNonPaymentValues(ps, policyPeriod)
      ps.addBatch()
      ps.executeBatch()
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "The RSR Entry is added to the Staging Table for PolicyPeriod : ${policyPeriod.PolicyNumber}")
      }
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Prepare the statment for the insert query for a RSR update. Here we will simply refer to the RSR update record that
   * has been saved.
   **/
  @Param("ps", PreparedStatement)
  @Param("rsrListUpdate", RSRListUpdate_Ext)
  private function prepareUpdateValues( ps : PreparedStatement, rsrListUpdate : RSRListUpdate_Ext ) {

    var METHOD_NAME = CLASS_NAME + "prepareUpdateValues"
    if ( _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + RSRConstants._methodIN )
    }

    var rsrPolicyHelper = new RSRPolicyHelper()

    ps.setString( 1, rsrListUpdate.PolicyNumber )                                                                       // 01 PolicyNumber,        varchar(50)
    ps.setString( 2, null )                                                                                             // 02 PolicyPublicID,      varchar(35)
    ps.setString( 3, rsrListUpdate.CreateUser.Credential.UserName )                                                     // 03 SenderID,            varchar(50)
    ps.setString( 4, rsrListUpdate.RegistrationCode as String )                                                                        // 04 RegistrationCode,    varchar(5)
    ps.setString( 5, RSRConstants.rsrVersion )                                                                          // 05 RSRVersion,          varchar(20)
    ps.setTimestamp( 6, _timeStamp )                                                                                    // 06 CreateTime,          datetime
    ps.setTimestamp( 7, _timeStamp )                                                                                    // 07 UpdateTime,          datetime
    ps.setString( 8, rsrListUpdate.RSRNumber )                                                                          // 08 RSRNumber,           varchar(50)
    ps.setString( 9, rsrListUpdate.InsuranceBranch )                                                                    // 09 InsuranceBranch,     varchar(20)
    ps.setString( 10, RSRConstants.FSMACode )                                                                           // 10 FSMACode,            varchar(40)
    ps.setString( 11, RSRData.convertDateEuroFormat( rsrListUpdate.EventDate ) )                                        // 11 EventDate,           varchar(15)
    ps.setString( 12, RSRData.convertDateEuroFormat( rsrListUpdate.EventIdentificationDate ) )                          // 12 RegistrationDate,    varchar(15)
    ps.setString( 13, rsrListUpdate.CodeReason.substring( 0, 2 ) )                                                      // 13 ReasonCode,          varchar(20)
    ps.setString( 14, rsrListUpdate.Dispute as String )                                                                 // 14 Disputed,            varchar(5)
    ps.setString( 15, rsrListUpdate.DoubtableAddress as String )                                                        // 15 DoubtableAddress,    varchar(5)
    ps.setString( 16, rsrListUpdate.Amount )                                                                            // 16 Amount,              varchar(50)
    ps.setString( 17, rsrListUpdate.Name )                                                                              // 17 Name,                varchar(50)
    var policyAddressString = "${rsrListUpdate.Street},${rsrListUpdate.HouseNumber},"
        + "${rsrListUpdate.BoxNumber},${rsrListUpdate.City},${rsrListUpdate.ZipCode}"
    ps.setString( 18, policyAddressString )                                                                             // 18 PolicyAddress,       varchar(max)
    ps.setString( 19, rsrListUpdate.LanguageCode as String )                                                            // 19 LanguageCode,        varchar(15)
    ps.setString( 20, rsrListUpdate.PersonCompany as String )                                                           // 20 EntityType,          varchar(15)
    if ( rsrListUpdate.PersonCompany == typekey.RSRPersonCompany_Ext.TC_1 ) {
      ps.setString( 21, rsrListUpdate.FirstName)                                                                        // 21 FirstName,           varchar(50)
      if ( rsrListUpdate.DateOfBirth != null ) {
        ps.setString( 22, RSRData.convertDateEuroFormat( rsrListUpdate.DateOfBirth ) )                                  // 22 DateOfBirth,         varchar(15)
      } else {
        ps.setNull( 22, null )                                                                                          // 22 DateOfBirth,         varchar(15)
      }
      if ( rsrListUpdate.Gender != null ) {
        ps.setString( 23, rsrListUpdate.Gender as String )                                                              // 23 Gender,              varchar(5)
      } else {
        ps.setString( 23, "0")                                                                                          // 23 Gender,              varchar(5)
      }
      ps.setNull( 24, null )                                                                                            // 24 JuridicalForm,       varchar(5)
      ps.setNull( 25, null )                                                                                            // 25 VATNumber,           varchar(50)
    }
    else if ( rsrListUpdate.PersonCompany == typekey.RSRPersonCompany_Ext.TC_2 ) {
      ps.setNull( 21, null )                                                                                            // 21 FirstName,           varchar(50)
      ps.setNull( 22, null )                                                                                            // 22 DateOfBirth,         varchar(15)
      ps.setNull( 23, null )                                                                                            // 23 Gender,              varchar(5)
      ps.setString( 24, rsrListUpdate.JuridicalForm as String )                                                         // 24 JuridicalForm,       varchar(5)
      if ( rsrListUpdate.VATNumber != null ) {
        ps.setString( 25, rsrListUpdate.VATNumber )                                                                     // 25 VATNumber,           varchar(50)
      } else {
        ps.setNull( 25, null )                                                                                          // 25 VATNumber,           varchar(50)
      }
    }
    ps.setString( 26, rsrListUpdate.Role as String )                                                                    // 26 Role,                varchar(15)
    var riskAddressString = "${rsrListUpdate.RiskStreet},${rsrListUpdate.RiskHouseNumber},${rsrListUpdate.RiskBoxNumber},"
        + "${rsrListUpdate.RiskCity},${rsrListUpdate.RiskZipCode}"
    ps.setString( 27, riskAddressString )                                                                               // 27 RiskLocationAddress, varchar(max)
    ps.setString( 28, null )                                                                                            // 28 ContactPublicID,     varchar(50)
    ps.setString( 29, rsrListUpdate.NumberClaims_RegisteredPerson )                                                     // 29 NumClaimsInvolved,   varchar(2)
    ps.setString( 30, rsrListUpdate.NumberClaims_RegisteredPerson )                                                     // 30 NumClaimsFault,      varchar(2)
    ps.setString( 31, rsrListUpdate.RSRDossierNumber )                                                                  // 31 RSRDossierNumber)    varchar(7)

    if ( _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + RSRConstants._methodOUT )
    }
  }

  /**
   * private function to insert the Data of policies cancelled 30 days ago to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("policyPeriod", PolicyPeriod)
  private function insertCancellationNonPaymentValues(ps : PreparedStatement, policyPeriod : PolicyPeriod) {

    var METHOD_NAME = CLASS_NAME + "insertCancellationNonPaymentValues"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var rsrPolicyHelper = new RSRPolicyHelper()
    ps.setString(1, policyPeriod.PolicyNumber)
    ps.setString(2, policyPeriod.PublicID)
    ps.setString(3,User.util.CurrentUser.Credential.UserName)
    ps.setString(4, RSRConstants.rsrRegisCode)
    ps.setString(5,RSRConstants.rsrVersion)
    ps.setTimestamp(6, new java.sql.Timestamp(currentDate.getTime()))
    ps.setTimestamp(7, new java.sql.Timestamp(currentDate.getTime()))
    ps.setString(8, rsrPolicyHelper.generateRSRNumber())
    //TODO This is a temporary solution.
    var productLine =  policyPeriod.policy.ProductCode
    ps.setString(9,getInsBranch(productLine))
    ps.setString(10, RSRConstants.FSMACode)
    ps.setString(11, RSRData.convertDateEuroFormat(policyPeriod.paymentDueDate))
    ps.setString(12, RSRData.convertDateEuroFormat(policyPeriod.CancellationDate))
    ps.setString(13, RSRConstants.reasonCode)
    ps.setString(14, RSRConstants.disputeRegis)
    ps.setString(15, RSRConstants.disputeRegis)
    ps.setString(16, policyPeriod.EstimatedPremium.Amount)
    ps.setString(18, policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine1+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine2+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine3+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.City+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.PostalCode)
    if(policyPeriod.PNIContactDenorm.LanguageCode != null) {
      ps.setString(19, policyPeriod.PNIContactDenorm.LanguageCode)
    } else {
      ps.setNull(19, null)
    }
    if(policyPeriod.PNIContactDenorm typeis Person ) {
      ps.setString(17,policyPeriod.PNIContactDenorm.LastName)
      ps.setString(20, "1")
      ps.setString(21, policyPeriod.PNIContactDenorm.FirstName)
      if(policyPeriod.PNIContactDenorm.DateOfBirth != null) {
        ps.setString(22, RSRData.convertDateEuroFormat(policyPeriod.PNIContactDenorm.DateOfBirth))
      } else {
        ps.setNull(22, null)
      }
      if(policyPeriod.PNIContactDenorm.Gender != null) {
        ps.setString(23,mapper.getAliasByInternalCode("GenderType", "RSR", policyPeriod.PNIContactDenorm.Gender.Code))
      } else {
        ps.setString(23, "0")
      }
      ps.setNull(24, null)
      ps.setNull(25, null)

    }
    else if(policyPeriod.PNIContactDenorm.Subtype == RSRConstants.company) {
      ps.setString(17, policyPeriod.PrimaryInsuredName)
      ps.setString(20, "2")
      ps.setNull(21, null)
      ps.setNull(22, null)
      ps.setNull(23, null)
      var accountOrgType = policyPeriod.Policy.Account.AccountOrgType.Code
      ps.setString(24,getOrgType(accountOrgType))
      if(policyPeriod.PNIContactDenorm.VATNum_Ext != null)
      {
        ps.setString(25,policyPeriod.PNIContactDenorm.VATNum_Ext.substring(3))
      }
      else
      {
        ps.setString(25,policyPeriod.PNIContactDenorm.VATNum_Ext)
      }
    }

    var contactRole = RSRConstants.roles.get(RSRConstants.insured) + RSRConstants.roles.get(RSRConstants.policyHolder)
    var policyCons = policyPeriod.getSlice(policyPeriod.EditEffectiveDate).AccountContactRoleMap
    var accCon = policyCons.keySet().where( \ elt -> elt.Contact.ID == policyPeriod.PNIContactDenorm.ID).first()
    var conRoles = policyCons.get(accCon).map(\ p -> (typeof p).TypeInfo.DisplayName ).join(", ")
    if(conRoles.containsIgnoreCase("driver")) {
      contactRole = contactRole + RSRConstants.roles.get("Driver")
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Contacts' -- ${policyPeriod.PNIContactDenorm.DisplayName} -- Role has Driver")
      }
    }
    ps.setString(26, contactRole)
    ps.setString(27,policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine1+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine2+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.AddressLine3+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.City+","+
        policyPeriod.PNIContactDenorm.PrimaryAddress.PostalCode)
    ps.setString(28, policyPeriod.PNIContactDenorm.PublicID)
    ps.setString(29, null)
    ps.setString(30, null)
    ps.setString(31, null)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * Function to Insert the Data to the Staging Table
   */
  @Param("policyPeriod", PolicyPeriod)
  @Param("xmlPayload", Object)
  public function insertToRSR(policyPeriod : PolicyPeriod, xmlPayload : Object) {
    var METHOD_NAME = CLASS_NAME + "insertToRSR"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    using (
        var con = getConnection(),
        var ps = con.prepareStatement(RSRDBQuery.getQuery("insertToRSRData"))) {
      //Insert new record for all operations except for cancel
      var policyContacts = getSelectedContact(xmlPayload)
      var policyCons = policyPeriod.AccountContactRoleMap
      if(_log.isDebugEnabled()) {
        _log.debug("${METHOD_NAME} The Policy Contacts Count is - ${policyCons.Count}")
      }
      for(pCon in policyContacts) {
        if(_log.isDebugEnabled()) {
          _log.debug(METHOD_NAME + "The RSR Entry is made for the Policiy Contact -- ${pCon.DisplayName} -- With Public ID -- (${pCon.PublicID})")
        }
        insertDefaultValues(ps, policyPeriod)
        insertUserEntryData(ps, xmlPayload)
        insertAddInsRow(ps, policyPeriod, pCon)
        var contactRole : String = null
        var accCon = policyCons.keySet().where( \ elt -> elt.Contact.ID == pCon.ID).first()
        var conRoles = policyCons.get(accCon).map(\ p -> (typeof p).TypeInfo.DisplayName ).join(", ")
        if(conRoles?.containsIgnoreCase("primary")) {
          contactRole = RSRConstants.roles.get("Insured") + RSRConstants.roles.get("PolicyHolder")
          if(_log.isDebugEnabled()) {
            _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Role is Insured and PolicyHolder")
          }
          if(policyPeriod.PolicyNumber == "Unassigned")  {
            contactRole = RSRConstants.roles.get("Prospect")
            if(_log.isDebugEnabled()) {
              _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Role is Prospect")
            }
          }
        }

        if(conRoles?.containsIgnoreCase("insured") and conRoles?.containsIgnoreCase("primary") == false) {
          contactRole = RSRConstants.roles.get("Insured")
          if(_log.isDebugEnabled()) {
            _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Role is Insured and PolicyHolder")
          }
        }

        if(conRoles.containsIgnoreCase("driver")) {
          contactRole = contactRole == null ? RSRConstants.roles.get("Driver") : contactRole + RSRConstants.roles.get("Driver")
          if(_log.isDebugEnabled()) {
            _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Role is Driver")
          }
        }

        ps.setString(26, contactRole)
        ps.addBatch()
      }
      if(policyPeriod.BusinessAutoLine != null){
        var comDrivers = getSelectedDriver(xmlPayload)
        for(driver in comDrivers) {
          insertDefaultValues(ps, policyPeriod)
          insertUserEntryData(ps, xmlPayload)
          ps.setString(17, driver.LastName)
          ps.setString(18, policyPeriod.PolicyAddress.AddressLine1+","+
              policyPeriod.PolicyAddress.AddressLine2+","+
              policyPeriod.PolicyAddress.AddressLine3+","+
              policyPeriod.PolicyAddress.City+","+
              policyPeriod.PolicyAddress.PostalCode)
          if(policyPeriod.PrimaryNamedInsured.ContactDenorm.LanguageCode != null) {
            ps.setString(19, policyPeriod.PrimaryNamedInsured.ContactDenorm.LanguageCode) // drivers Primary Language will be recorded as Primary Insureds' Primary Language since Mandatory at RSR
          } else {
            ps.setNull(19, null)
          }
          ps.setString(20, "1")
          ps.setString(21, driver.FirstName)
          if(driver.DateOfBirth != null) {
            ps.setString(22, RSRData.convertDateEuroFormat(driver.DateOfBirth))
          } else {
            ps.setNull(22, null)
          }
          if(driver.Gender != null) {
            ps.setString(23,mapper.getAliasByInternalCode("GenderType", "RSR", driver.Gender.Code))
          } else {
            ps.setString(23, "0") // If the Gender is Null - unknown is sent to RSR
          }
          ps.setNull(24, null)
          ps.setNull(25, null)
          ps.setString(27,policyPeriod.PolicyAddress.AddressLine1+","+
              policyPeriod.PolicyAddress.AddressLine2+","+
              policyPeriod.PolicyAddress.AddressLine3+","+
              policyPeriod.PolicyAddress.City+","+
              policyPeriod.PolicyAddress.PostalCode) //TODO The Policy Holder's Address shall be defaulted for now
          ps.setString(28, driver.PublicID )
          ps.setString(26, RSRConstants.roles.get("Driver"))

          ps.addBatch()
        }
      }
      ps.executeBatch()
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "The RSR Entry is added to the Staging Table for PolicyPeriod : ${policyPeriod.PolicyNumber}")
      }
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * Function to get the values available in the staging table
   */
  @Returns ("List of records in Staging Table")
  public function getPolicies() : List<RSRData> {
    var METHOD_NAME = CLASS_NAME + "getPolicies"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var list = new List<RSRData>()
    var ps = getConnection().prepareStatement(RSRDBQuery.getQuery("getRSRPolicyData"))
    using(var rs = ps.executeQuery()) {
      while (rs.next()) {
        var map = new HashMap<RSRField, String>()
        map.put(RSRField.PolicyHolderName, rs.getString(1))
        var addr = rs.getString(2).split(",").toList()
        var amount : StringBuilder = ""
        var FSMACode : StringBuilder = ""
        map.put(RSRField.PolicyHolderStreet,addr[0])
        map.put(RSRField.PolicyHolderHouseNum,addr[1])
        if(addr[2].toString().equals("null"))
        {
          map.put(RSRField.PolicyHolderBoxNum,"    ")
        }
        else
        {
          map.put(RSRField.PolicyHolderBoxNum,addr[2])
        }
        map.put(RSRField.PolicyHolderZipCode,addr[4])
        map.put(RSRField.PolicyHolderCity,addr[3])
        map.put(RSRField.LangCode,rs.getString(3))
        var entityType = rs.getString(4)
        map.put(RSRField.EntityType,entityType)
        if(entityType == "1") {
          map.put(RSRField.FirstName,rs.getString(5))
          map.put(RSRField.DOB,rs.getString(6))
          map.put(RSRField.Gender,rs.getString(7))
        } else {
          map.put(RSRField.JuridicalForm,rs.getString(8))
          map.put(RSRField.VATNum,rs.getString(9))
        }
        var ins_br = rs.getString(12)
        if(ins_br.startsWith("3"))
        {
          var riskaddr = rs.getString(10).split(",").toList()
          if(riskaddr.Count >= 4) {
            if(riskaddr[0].toString().equals("null"))
            {
              map.put(RSRField.RiskLocStreet,"                              ")
            }
            else
            {
              map.put(RSRField.RiskLocStreet,riskaddr[0])
            }
            if(riskaddr[1].toString().equals("null"))
            {
              map.put(RSRField.RiskLocHouseNum,"     ")
            }
            else
            {
              map.put(RSRField.RiskLocHouseNum,riskaddr[1])
            }
            if(riskaddr[2].toString().equals("null"))
            {
              map.put(RSRField.RiskLocBoxNum,"    ")
            }
            else
            {
              map.put(RSRField.RiskLocBoxNum,riskaddr[2])
            }
            if(riskaddr[4].toString().equals("null"))
            {
              map.put(RSRField.RiskLocZip,"       ")
            }
            else
            {
              map.put(RSRField.RiskLocZip,riskaddr[4])
            }
            if(riskaddr[3].toString().equals("null"))
            {
              map.put(RSRField.RiskLocCity,"                        ")
            }
            else
            {
              map.put(RSRField.RiskLocCity,riskaddr[3])
            }
          } else {
            map.put(RSRField.RiskLocStreet,riskaddr[0])
          }
        }
        map.put(RSRField.PersonRole,rs.getString(11))
        map.put(RSRField.InsAndSubInsBranch,ins_br)
        map.put(RSRField.RegistrationCode,rs.getString(13))
        FSMACode = rs.getString(14)
        FSMACode = getFSMACode(FSMACode)
        map.put(RSRField.FSMACode,FSMACode)
        map.put(RSRField.RSRPolicyNumber,rs.getString(15)) //PolicyNumber in PC
        map.put(RSRField.SenderInfo,rs.getString(16))
        map.put(RSRField.EventDate,rs.getString(17))
        map.put(RSRField.RegistrationDate,rs.getString(18))
        map.put(RSRField.ReasonCode,rs.getString(19) )
        map.put(RSRField.RegistrationCode,rs.getString(20) )
        map.put(RSRField.Disputed,rs.getString(21) )
        map.put(RSRField.DoubtableAddr,rs.getString(22) )
        amount  = rs.getString(23)
        if(amount != null)
        {
          while(amount.length() < 9)
          {
            (amount).insert(0,"0").toString();

          }
        }
        map.put(RSRField.Amount,amount)
        map.put(RSRField.Version,rs.getString(24))
        map.put(RSRField.RSRNumber,rs.getString(25))
        map.put(RSRField.RSRDossierNumber,rs.getString(31))
        map.put(RSRField.NoOfClaims,rs.getString(26))
        map.put(RSRField.NoOfClaimsInfault,rs.getString(27))
        list.add(new RSRData(map.copy()))
      }
    }
    if(_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "The Contacts Count from Staging Table is -- (${list.Count})")
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return list
  }

  /**
   * public function to empty Staging Table
   */
  @Returns("flag depending on whether delete was successful or not")
  public function deleteRecordsStagingTable() : int {
    var flag =0
    var METHOD_NAME = CLASS_NAME + "deleteRecordsStagingTable"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    try{
      var ps = getConnection().prepareStatement(RSRDBQuery.getQuery("deleteStagingData"))
      ps.addBatch()
      ps.executeBatch()
      flag = 1
    }
        catch(ex: Exception)
        {
          flag=0
        }
    return flag
  }


  /**
   * public function to add the RSR Data from Staging Table to records_sent_datassur_ext table
   */
  @Returns("flag depending on whether add was successful or not")
  public function getRecordsDatassurPolicies() : int {
    var METHOD_NAME = CLASS_NAME + "getRecordsDatassurPolicies"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var flag = 0
    var result = 0
    var list = new List<RSRData>()
    try{
      var ps = getConnection().prepareStatement(RSRDBQuery.getQuery("getRSRPolicyData"))
      using(var rs = ps.executeQuery()) {
        gw.transaction.Transaction.runWithNewBundle(\bundle -> {
          while (rs.next()) {
            var policyholderName = rs.getString(1)
            var policyaddr =    rs.getString(2)
            var langcode =    rs.getString(3)
            var entitytype =    rs.getString(4)
            var firstname =    rs.getString(5)
            var DOB = rs.getString(6)
            var gender =    rs.getString(7)
            var orgtype =    rs.getString(8)
            var vatnumber =    rs.getString(9)
            var ristLocAddr =    rs.getString(10)
            var PRole = rs.getString(11)
            var ins_branch =    rs.getString(12)
            var regis_code =    rs.getString(13)
            var FSMACode =    rs.getString(14)
            var POLICYNUMBER =    rs.getString(15)
            var senderid = rs.getString(16)
            var eventdate =    rs.getString(17)
            var regis_date =    rs.getString(18)
            var reasoncode =    rs.getString(19)
            var isdispute =    rs.getString(21)
            var doubtableaddr = rs.getString(22)
            var amount =    rs.getString(23)
            var rsrversion =    rs.getString(24)
            var RSRnumber =    rs.getString(25)
            var NumClaimsInvolved =    rs.getString(26)
            var NumClaimsFault = rs.getString(27)
            var policyPublicId =    rs.getString(28)
            var contactPublicId =    rs.getString(29)
            var multiConInd = rs.getString(30)
            var rsrDossierNumber = rs.getString(31)


            var rsrDatassurRecord = new Records_sent_Datassur_Ext()
            bundle.add(rsrDatassurRecord)
            rsrDatassurRecord.PolicyNumber = POLICYNUMBER
            rsrDatassurRecord.Name = policyholderName
            rsrDatassurRecord.RSRNumber =  RSRnumber
            rsrDatassurRecord.Policyaddr = policyaddr
            rsrDatassurRecord.FirstName = firstname
            rsrDatassurRecord.VatNumber = vatnumber
            rsrDatassurRecord.SenderId = senderid
            rsrDatassurRecord.FSMACode = FSMACode
            rsrDatassurRecord.RSRVersion =  rsrversion
            rsrDatassurRecord.RiskLocAddr = ristLocAddr
            rsrDatassurRecord.LangCode = langcode
            rsrDatassurRecord.EntityType = entitytype
            rsrDatassurRecord.Gender = gender
            rsrDatassurRecord.PRole = PRole
            rsrDatassurRecord.OrgType = orgtype
            rsrDatassurRecord.InsBranch = ins_branch
            rsrDatassurRecord.ReasonCode = reasoncode
            rsrDatassurRecord.RegisCode = regis_code
            rsrDatassurRecord.IsDispute = isdispute
            rsrDatassurRecord.DoubtableAddr = doubtableaddr
            rsrDatassurRecord.Amount = amount
            rsrDatassurRecord.DOB=stringToDateConversion(DOB)
            rsrDatassurRecord.EventDate = stringToDateConversion(eventdate)
            rsrDatassurRecord.RegisDate = stringToDateConversion(regis_date)
            rsrDatassurRecord.NumClaimsInvolved = NumClaimsInvolved
            rsrDatassurRecord.NumClaimsFault = NumClaimsFault
            rsrDatassurRecord.PolicyPublicId = policyPublicId
            rsrDatassurRecord.ContactPublicId = contactPublicId
            rsrDatassurRecord.MultiConInd = multiConInd
            rsrDatassurRecord.RSRDossierNumber = rsrDossierNumber
          }
        }, User.util.CurrentUser.Credential.UserName)
        flag = 1
        var Records_sent_DatassurListQuery1 = gw.api.database.Query.make(Records_sent_Datassur_Ext)
        result = Records_sent_DatassurListQuery1.select().Count
      }
    }
        catch(ef:Exception)
        {
          flag=0
        }
    if(_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "The Records_sent_DatassurListQuery Count is -- "+result)
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return flag
  }



  /**
   * private function to insert the RSR Data to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("xml", gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod)
  private function insertCancellationData(ps : PreparedStatement, xml : gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "insertCancellationData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    ps.setString(3, xml.Cancellation.RSRData_ExtID.CreateUser.Credential.UserName)
    ps.setString(8, xml.Cancellation.RSRData_ExtID.RSRNumber)

    var ins_nd_sub = policyHelper.getInsuranceValues(xml.Cancellation.RSRData_ExtID.RSRInsBranch,
        xml.Cancellation.RSRData_ExtID.RSRSubBranch)
    ps.setString(9,ins_nd_sub) //TODO This may need changes since multiple Insurance branches can be selected for an event. Current design shall allow one selection only
    ps.setString(10,xml.Cancellation.RSRData_ExtID.FSMACode)
    ps.setString(11, RSRData.convertDateEuroFormat(xml.Cancellation.RSRData_ExtID.EventDate))
    ps.setString(12, RSRData.convertDateEuroFormat(xml.Cancellation.RSRData_ExtID.RegisDate))
    ps.setString(13,xml.Cancellation.RSRData_ExtID.RsrReasonCode)
    ps.setString(14, "2")                              // Dispute value 2 always as discussed
    if(xml.Cancellation.RSRData_ExtID.DoubtableAddr) {
      ps.setString(15,"1")
    } else {
      ps.setString(15,"2")
    }
    ps.setString(16, xml.Cancellation.RSRData_ExtID.RSRAmount)
    ps.setString(29, xml.Cancellation.RSRData_ExtID.NumClaimsInvolved)
    ps.setString(30, xml.Cancellation.RSRData_ExtID.NumClaimsFault)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * private function to insert the RSR Data to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("xml", gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod)
  private function insertChangeData(ps : PreparedStatement, xml : gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "insertChangeData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    ps.setString(3, xml.PolicyChange.RSRPolicydata_ExtID.CreateUser.Credential.UserName)
    ps.setString(8, xml.PolicyChange.RSRPolicydata_ExtID.RSRNumber)
    var ins_nd_sub = policyHelper.getInsuranceValues(xml.PolicyChange.RSRPolicydata_ExtID.RSRInsBranch,
        xml.PolicyChange.RSRPolicydata_ExtID.RSRSubBranch)
    ps.setString(9,ins_nd_sub) //TODO This may need changes since multiple Insurance branches can be selected for an event. Current design shall allow one selection only
    ps.setString(10,xml.PolicyChange.RSRPolicydata_ExtID.FSMACode)
    ps.setString(11, RSRData.convertDateEuroFormat(xml.PolicyChange.RSRPolicydata_ExtID.EventDate))
    ps.setString(12, RSRData.convertDateEuroFormat(xml.PolicyChange.RSRPolicydata_ExtID.RegisDate))
    ps.setString(13,xml.PolicyChange.RSRPolicydata_ExtID.RsrReasonCode)
    ps.setString(14, "2")                                       // dispute value 2 always as discussed
    if(xml.PolicyChange.RSRPolicydata_ExtID.DoubtableAddr) {
      ps.setString(15,"1")
    } else {
      ps.setString(15,"2")
    }
    ps.setString(16, xml.PolicyChange.RSRPolicydata_ExtID.RSRAmount)
    ps.setString(29, xml.PolicyChange.RSRPolicydata_ExtID.NumClaimsInvolved)
    ps.setString(30, xml.PolicyChange.RSRPolicydata_ExtID.NumClaimsFault)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * private function to insert the RSR Data to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("xml", gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod)
  private function insertSubmissionData(ps : PreparedStatement, xml : gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "insertSubmissionData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    ps.setString(3, xml.Submission.RSRSubPolicydata_ExtID.CreateUser.Credential.UserName)
    ps.setString(8, xml.Submission.RSRSubPolicydata_ExtID.RSRNumber)
    ps.setString(10,xml.Submission.RSRSubPolicydata_ExtID.FSMACode)
    var ins_nd_sub = policyHelper.getInsuranceValues(xml.Submission.RSRSubPolicydata_ExtID.RSRInsBranch,
        xml.Submission.RSRSubPolicydata_ExtID.RSRSubBranch)
    ps.setString(9,ins_nd_sub) //TODO This may need changes since multiple Insurance branches can be selected for an event. Current design shall allow one selection only
    ps.setString(11, RSRData.convertDateEuroFormat(xml.Submission.RSRSubPolicydata_ExtID.EventDate))
    ps.setString(12, RSRData.convertDateEuroFormat(xml.Submission.RSRSubPolicydata_ExtID.RegisDate))
    ps.setString(13,xml.Submission.RSRSubPolicydata_ExtID.RsrReasonCode)
    ps.setString(14, "2")                                 // Dispute value 2 always as discussed
    if(xml.Submission.RSRSubPolicydata_ExtID.DoubtableAddr) {
      ps.setString(15,"1")
    } else {
      ps.setString(15,"2")
    }
    ps.setString(16, xml.Submission.RSRSubPolicydata_ExtID.RSRAmount)
    ps.setString(29, xml.Submission.RSRSubPolicydata_ExtID.NumClaimsInvolved)
    ps.setString(30, xml.Submission.RSRSubPolicydata_ExtID.NumClaimsFault)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * private function to insert the Default Data to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("policyPeriod", PolicyPeriod)
  private function insertDefaultValues(ps : PreparedStatement, policyPeriod : PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "insertDefaultValues"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    if(policyPeriod.PolicyNumber != null)
    {
      ps.setString(1, policyPeriod.PolicyNumber)
    }
    else
    {
      ps.setString(1, policyPeriod.Job.JobNumber)
    }
    ps.setString(2, policyPeriod.PublicID)
    ps.setString(4, "1")  // TODO RSR Registration Code -- Will Need Modifications
    ps.setString(5, RSRConstants.rsrVersion)
    ps.setTimestamp(6, new java.sql.Timestamp(currentDate.getTime()))
    ps.setTimestamp(7, new java.sql.Timestamp(currentDate.getTime()))
    ps.setString(31,null)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * private function to insert the RSR Data for
   * Additional Insureds to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("policyPeriod", PolicyPeriod)
  @Param("pCon", Contact)
  private function insertAddInsRow(ps : PreparedStatement, policyPeriod : PolicyPeriod,
                                   pCon : Contact ) {
    var METHOD_NAME = CLASS_NAME + "insertAddInsRow"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    ps.setString(18, pCon.PrimaryAddress.AddressLine1+","+
        pCon.PrimaryAddress.AddressLine2+","+
        pCon.PrimaryAddress.AddressLine3+","+
        pCon.PrimaryAddress.City+","+
        pCon.PrimaryAddress.PostalCode)
    if(pCon.LanguageCode != null) {
      ps.setString(19, pCon.LanguageCode.Code)
    } else {
      ps.setNull(19, null)
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Language Code is Null")
      }
    }
    var accountOrgType = ""
    var con = pCon
    if(con typeis Person ) {
      ps.setString(17,con.LastName)
      ps.setString(20, "1")
      ps.setString(21, con.FirstName)
      if(con.DateOfBirth != null) {
        ps.setString(22, RSRData.convertDateEuroFormat(con.DateOfBirth))
      } else {
        ps.setNull(22, null)
      }
      if(con.Gender != null) {
        ps.setString(23,mapper.getAliasByInternalCode("GenderType", "RSR", con.Gender.Code))
      } else {
        ps.setString(23, "0") // TODO If the Gender is Null - unknown is sent to RSR
      }
      ps.setNull(24, null)
      ps.setNull(25, null)
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Subtype is Person")
      }
    } else if(con.Subtype == "Company") {
      ps.setString(17, con.DisplayName)
      ps.setString(20, "2")
      ps.setNull(21, null)
      ps.setNull(22, null)
      ps.setNull(23, null)
      accountOrgType = policyPeriod.Policy.Account.AccountOrgType.Code
      ps.setString(24,getOrgType(accountOrgType))
      if(con.VATNum_Ext != null)
      {
        ps.setString(25,con.VATNum_Ext.substring(3))
      }
      else
      {
        ps.setString(25,con.VATNum_Ext)
      }
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Subtype is Company")
      }
    } else {
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Contacts' -- ${pCon.DisplayName} -- Subtype is ::  ${typeof pCon}")
      }
    }

    ps.setString(27,pCon.PrimaryAddress.AddressLine1+","+
        pCon.PrimaryAddress.AddressLine2+","+
        pCon.PrimaryAddress.AddressLine3+","+
        pCon.PrimaryAddress.City+","+
        pCon.PrimaryAddress.PostalCode) //For now The Policy Holder's Address shall be defaulted
    ps.setString(28, pCon.PublicID)
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }


  /**
   * private function to insert the User Entry RSR Data to Staging Table
   */
  @Param("ps", PreparedStatement)
  @Param("xmlPayload", Object)
  private function insertUserEntryData( ps : PreparedStatement, xmlPayload : Object) {
    var METHOD_NAME = CLASS_NAME + "insertUserEntryData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    if(xmlPayload typeis gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod) {
      insertCancellationData(ps, xmlPayload)
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Cancellation")
      }
    } else if (xmlPayload typeis gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod){
      insertChangeData(ps, xmlPayload)
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Change")
      }
    } else if(xmlPayload typeis gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod) {
      insertSubmissionData(ps, xmlPayload)
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Refusal")
      }
    } else {
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Some Error occured during the RSR Entry")
      }
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * private function to get the list of Contacts to send the RSR Report
   */
  @Param("xmlPayload", Object)
  @Returns("List of Contacts")
  private function getSelectedContact(xmlPayload : Object) : List<Contact> {
    var METHOD_NAME = CLASS_NAME + "insertUserEntryData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var retVal : String = null
    var cons = new List<Contact>()

    if(xmlPayload typeis gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod) {
      retVal = xmlPayload.Cancellation.RSRData_ExtID.ContactID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Cancellation")
      }
    } else if (xmlPayload typeis gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod){
      retVal = xmlPayload.PolicyChange.RSRPolicydata_ExtID.ContactID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Change")
      }
    } else if(xmlPayload typeis gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod) {
      retVal = xmlPayload.Submission.RSRSubPolicydata_ExtID.ContactID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Refusal")
      }
    } else {
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Some Error occured during the RSR Entry")
      }
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    var retList = retVal?.split(RSRConstants.CON_Separator)
    var contactQuery =  gw.api.database.Query.make(Contact).select()
    for (contact in contactQuery)
    {
      for (publicId in retList)
      {
        if(contact.PublicID == publicId)
        {
          cons.add(contact)
        }
      }
    }
    return cons.Count > 0 ? cons : {}
  }


  /**
   * private function to get the Commercial Driver List
   */
  @Param("xmlPayload", Object)
  @Returns("List of CommercialDrivers")
  private function getSelectedDriver(xmlPayload : Object) : List<CommercialDriver> {
    var METHOD_NAME = CLASS_NAME + "insertUserEntryData"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var retVal : String = null
    var cons = new List<CommercialDriver>()

    if(xmlPayload typeis gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod) {
      retVal = xmlPayload.Cancellation.RSRData_ExtID.ComDriverID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Cancellation")
      }
    } else if (xmlPayload typeis gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod){
      retVal = xmlPayload.PolicyChange.RSRPolicydata_ExtID.ComDriverID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Change")
      }
    } else if(xmlPayload typeis gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod) {
      retVal = xmlPayload.Submission.RSRSubPolicydata_ExtID.ComDriverID
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "RSR Entry is made via Policy Refusal")
      }
    } else {
      if(_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "Some Error occured during the RSR Entry")
      }
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    var retList = retVal?.split(RSRConstants.CON_Separator)
    var commercialDriverQuery =  gw.api.database.Query.make(CommercialDriver).select()
    for (commercialDriver in commercialDriverQuery)
    {
      for (publicId in retList)
      {
        if(commercialDriver.PublicID == publicId)
        {
          cons.add(commercialDriver)
        }
      }
    }
    return cons.Count > 0 ? cons : {}

  }

  /**
   * Convert String to Date
   */
  @Param("dateString", String)
  @Returns("Date in a specific format")
  private  function stringToDateConversion(dateString: String): Date {
    var dateFormatter = new SimpleDateFormat("ddMMyyyy")
    if ((dateString != null) and (dateString !="        ")) {
      return (dateFormatter.parse(dateString) )
    }
    else
      return null
  }

  /**
   * Get InsBranch for Automatic Batch
   */
  @Param("productCode", String)
  @Returns("InsuranceBranch for Automatic batch")
  private function getInsBranch(productCode : String) : String
  {
    var insBranch = ""
    switch(productCode){
      case RSRConstants.productLineBO :
          insBranch = RSRConstants.insBranchIM
          break;
      case RSRConstants.productLineIM :
          insBranch = RSRConstants.insBranchIM
          break;
      case RSRConstants.productLineCPP :
          insBranch = RSRConstants.insBranchIM
          break;
      case RSRConstants.productLineCPK :
          insBranch = RSRConstants.insBranchIM
          break;
      case RSRConstants.productLineWC :
          insBranch = RSRConstants.insBranchWC
          break;
      case RSRConstants.productLinePA :
          insBranch = RSRConstants.insBranchPA
          break;
      case RSRConstants.productLineBA :
          insBranch = RSRConstants.insBranchPA
          break;
      case RSRConstants.productLineGL :
          insBranch = RSRConstants.insBranchGL
          break;
        default :
    }
    return insBranch
  }

  /**
   * Get OrganisationType / Juridical Form for Company
   */
  @Param("accountOrgType", AccountOrgType)
  @Returns("Organisation Type / Juridical Form of company")
  private function getOrgType(accountOrgType : AccountOrgType) : String
  {
    var orgType = ""
    switch(accountOrgType)
    {
      case AccountOrgType.TC_INDIVIDUAL :
          orgType = RSRConstants.orgTypeIndividual
          break;
      case AccountOrgType.TC_SOLEPROPSHIP :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_PARTNERSHIP :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_CORPORATION :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_PRIVATECORP :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_LLC :
          orgType = RSRConstants.orgTypeLLC
          break;
      case AccountOrgType.TC_JOINTVENTURE :
          orgType = RSRConstants.orgTypeJointVenture
          break;
      case AccountOrgType.TC_COMMONOWNERSHIP :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_LIMITEDPARTNERSHIP :
          orgType = RSRConstants.orgTypeLLC
          break;
      case AccountOrgType.TC_TRUSTESTATE :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_EXECUTORTRUSTEE :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_LLP :
          orgType = RSRConstants.orgTypeLLC
          break;
      case AccountOrgType.TC_GOVERNMENT :
          orgType = RSRConstants.orgTypeGovernment
          break;
      case AccountOrgType.TC_NONPROFIT :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_RELIGIOUS :
          orgType = RSRConstants.orgTypeOther
          break;
      case AccountOrgType.TC_OTHER :
          orgType = RSRConstants.orgTypeOther
          break;
        default :
        orgType = RSRConstants.orgTypeOther
        break;
    }
    return orgType
  }


  /**
   * Gives FSMACode
   */
  @Param("FSMACode", StringBuilder)
  @Returns("FSMACode for RSR file to be sent to Datassur")
  private function getFSMACode(FSMACode : StringBuilder) : String
  {
    var len = FSMACode.length()
    if(len >= 4)
    {
      FSMACode = FSMACode.substring(len-4)
      (FSMACode).insert(0,"0").toString();
    }
    else if(len < 4)
    {
      while(len<5)
      {
        (FSMACode).insert(0,"0")
        len++
      }
    }
    return FSMACode.toString()
  }
}