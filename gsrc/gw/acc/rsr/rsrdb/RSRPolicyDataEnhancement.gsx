package gw.acc.rsr.rsrdb

uses gw.acc.rsr.RSRConstants
uses gw.util.GosuStringUtil
uses gw.acc.rsr.RSRPolicyHelper

/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 13/11/15
 * Time: 2:41 PM
 * Enhancement on RSRPolicyData_Ext to support RSR Screen
 */
enhancement RSRPolicyDataEnhancement: entity.RSRPolicyData_Ext {
  /**
   * Function to set the RSR Insurance Value from the PCF
   */
  function setInsValue(ins: RSRInsuranceBranch_Ext[]) {
    ins.each(\elt -> {
      if (this.RSRInsBranch == null) {
        this.RSRInsBranch = elt.Code
        RSRConstants._log.debug(" :: Insurance Branch Selected :: " + this.RSRInsBranch)
      } else {
        this.RSRInsBranch = this.RSRInsBranch + ":" + elt.Code
        RSRConstants._log.debug(" :: Insurance Branch Selected :: " + this.RSRInsBranch)
      }
    })
  }

  /**
   * Function to set the RSR Insurance Sub-Branch Value from the PCF
   */
  function setInssubValue(inssub: RSRInsSub_Branch_Ext[]) {
    if (inssub.Count == 0) {
      this.RSRSubBranch = null
    }
    inssub?.each(\elt -> {
      if (this.RSRSubBranch == null) {
        this.RSRSubBranch = elt.Code

        RSRConstants._log.debug("Insurance Sub Branch Selected :: " + this.RSRInsBranch)
      } else {
        if (this.RSRSubBranch.containsIgnoreCase(elt) == false) {
          this.RSRSubBranch = this.RSRSubBranch + ":" + elt.Code
        }
        RSRConstants._log.debug("Insurance Sub Branch Selected :: " + this.RSRInsBranch)
      }
    })

    if (inssub.Count >= 6) {
      if (inssub.first().Categories.first().Code == "fire") {
        this.RSRSubBranch = "3"
      } else if (inssub.first().Categories.first().Code == "car") {
        this.RSRSubBranch = "5"
      } else if (inssub.first().Categories.first().Code == "civilliability") {
        this.RSRSubBranch = "4"
      }
    }
  }

  /**
   * Function to get the list of RSR Sub branches available
   * for the RSR Insurance Branch selection
   */
  @Returns("List of Sub branches based on category")
  @Param("insBranch", RSRInsuranceBranch_Ext)
  function getInsSubList(insBranch: RSRInsuranceBranch_Ext): List<RSRInsSub_Branch_Ext> {
    var retValue = new List<RSRInsSub_Branch_Ext>()
    if (insBranch == null) {
      return {}
    } else {
      retValue.addAll(RSRInsSub_Branch_Ext.getTypeKeys(true).where(\elt -> elt.hasCategory(insBranch)))
    }
    return retValue
  }

  function getInitialVal(): List<RSRInsSub_Branch_Ext> {
    var retValue = new List<RSRInsSub_Branch_Ext>()

    if (this.RSRSubBranch == null) {
      return {}
    } else {
      this.RSRSubBranch.split(":").each(\elt -> {
        retValue.add(RSRInsSub_Branch_Ext.get(elt))
      })
    }

    return retValue
  }

  function toRemove(inssub: RSRInsSub_Branch_Ext[]) {
    if (inssub.Count > 0 and this.RSRSubBranch != null) {
      var sub = inssub.where(\elt1 -> elt1.Code.containsIgnoreCase(this.RSRSubBranch) == false)
      this.RSRSubBranch = this.RSRSubBranch.remove(sub.Code)
    }
  }

  function setContactID(contacts: Contact[]) {
    this.ContactID = null
    for (contact in contacts) {
      if (this.ContactID == null) {
        this.ContactID = contact.PublicID
      } else if (this.ContactID.containsIgnoreCase(contact.PublicID) == false) {
        this.ContactID = this.ContactID + RSRConstants.CON_Separator + contact.PublicID
      }
    }
  }

  function setContactID(drivers: CommercialDriver[]) {
    this.ComDriverID = null
    for (contact in drivers) {
      if (this.ComDriverID == null) {
        this.ComDriverID = contact.PublicID
      } else if (this.ComDriverID.containsIgnoreCase(contact.PublicID) == false) {
        this.ComDriverID = this.ComDriverID + RSRConstants.CON_Separator + contact.PublicID
      }
    }
  }

  function isSelected(con: Contact): String {
    var retVal: String = GosuStringUtil.EMPTY
    if (this.ContactID != null and this.ContactID.Empty == false) {
      if (this.ContactID?.containsIgnoreCase(con.PublicID)){
        retVal = "Selected"
      }
    }

    return retVal
  }

  function isSelected(con: CommercialDriver): String {
    var retVal: String = GosuStringUtil.EMPTY
    if (this.ComDriverID != null and this.ComDriverID.Empty == false) {
      if (this.ComDriverID?.containsIgnoreCase(con.PublicID)){
        retVal = "Selected"
      }
    }

    return retVal
  }

  /*
   Function to reset sub branch on change of insurance branch
   */

  function resetSubBranch() {
    this.RSRSubBranch = null
  }

  /*
   Function to return name of given subBranch Code
   */

  @Returns("Name of given subBranch Code")
  @Param("subBranches", String)
  function getSubBranchDesc(subBranches: String): String {
    var list = subBranches?.split(RSRConstants.SUBBRANCH_CODE_SEPARATOR).toList()
    var subBranchDesc: String = GosuStringUtil.EMPTY
    for (subBranch in list index i) {
      subBranchDesc = subBranchDesc + typekey.RSRInsSub_Branch_Ext.get(subBranch).DisplayName
      if (i < list.size() - 1){
        subBranchDesc = subBranchDesc + RSRConstants.SUBBRANCH_SEPARATOR
      }
    }
    return subBranchDesc
  }

  /*
   Function to set RSR Number on opening of popup
   */

  function setRSRNumber() {
    var rsrHelper = new RSRPolicyHelper()
    this.RSRNumber = rsrHelper.generateRSRNumber()
  }
}
