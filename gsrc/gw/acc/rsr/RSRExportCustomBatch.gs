package gw.acc.rsr

uses gw.processes.BatchProcessBase
uses java.io.File
uses java.lang.Exception
uses java.lang.IllegalArgumentException
uses java.util.Date
uses gw.acc.rsr.rsrdb.RSRDBBase
uses org.slf4j.Logger
uses gw.api.database.DBFunction
uses util.IntegrationPropertiesUtil
/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 28/10/15
 * Time: 6:01 PM
 * To change this template use File | Settings | File Templates.
 */

class RSRExportCustomBatch extends BatchProcessBase {

  public static final var _log: Logger = RSRConstants._log
  static final var CLASS_NAME = "RSRExportCustomBatch :::"
  public var currentDate : Date = gw.api.util.DateUtil.currentDate()


  /**
   * To construct the Batch Process
   */
  construct() {
    super(BatchProcessType.TC_RSREXPORT_EXT)
  }


  /**
   * Override Function to check the Initial Conditions
   */
  override function checkInitialConditions(): boolean {
    if (!new File(IntegrationPropertiesUtil.getProperty("RSRExportPath")).exists()) {
      _log.error("RSR Export Path directory not found ${IntegrationPropertiesUtil.getProperty("RSRExportPath")}")
      throw new IllegalArgumentException("Incorrect RSRExportPath: ${IntegrationPropertiesUtil.getProperty("RSRExportPath")}")
    }
    return true
  }


  /**
   * Do Work of the Batch Process Actual - To Create the RSR File with the Data.
   */
  override function doWork() {
    var METHOD_NAME = CLASS_NAME + "doWork"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    OperationsExpected = 0
    var createFile = 0
    var insertRecordsDatassur = 0
    var deleteRacordsStaging = 0
    var successFlag  = 0
    //all or nothing approach - if task fails - the file will be deleted
    try {

      //treat termination as exception and delete file afterwards
      if (TerminateRequested) {
        throw new Exception("Terminate Requested!")
      }
      var stagingDB = new RSRDBBase()
      var records = stagingDB.getPolicies()
      OperationsExpected = records.Count

      if(_log. isDebugEnabled()) {
        _log.trace(METHOD_NAME + " ****OperationsExpected****" + OperationsExpected)
      }
      createFile = RSRPolicyMessagingUtil.writeData(records)
      if(createFile==1) {
        insertRecordsDatassur = stagingDB.getRecordsDatassurPolicies()
      }
      if(insertRecordsDatassur==1)
      {
        deleteRacordsStaging = stagingDB.deleteRecordsStagingTable()
      }
      successFlag = checkDataIntegrity(createFile , insertRecordsDatassur , deleteRacordsStaging)
      if(_log. isTraceEnabled()) {
        _log.trace(METHOD_NAME + " success{1}/failure{0} "+ successFlag)
      }
    } catch (ex: Exception) {
      incrementOperationsFailed()
      OperationsFailedReasons.add(ex.Message)
      _log.error("Export failed - deleting file", ex +":::" +ex.StackTraceAsString)
      _log.error("Export failed - Cleaning Table", ex +":::" +ex.StackTraceAsString)
      rollBack()
    } finally {
      incrementOperationsCompleted()
    }
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Support termination before each query export in the iteration
   */
  override function requestTermination(): boolean {
    return true
  }

  /**
   * Delete file if one of operations fails
   */
  private function deleteFile() {
    try {
      new File(RSRData.FilePath).delete()
    } catch (ef:Exception) {
      _log.error("Export failed - FILE DELETE FAILED!!!", ef)
      OperationsFailedReasons.add(ef.Message)
    }
  }

  /**
   *  Cleans the Staging Table
   */
  private function cleanTable() {
    var records_sent_Datassur_ExtQuery = gw.api.database.Query.make(Records_sent_Datassur_Ext)
    var count =   records_sent_Datassur_ExtQuery.select().Count
    records_sent_Datassur_ExtQuery.compare(DBFunction.DateFromTimestamp(
        records_sent_Datassur_ExtQuery.getColumnRef("CreateTime")),
        Equals, DateTime.Today)
    var result = records_sent_Datassur_ExtQuery.select()
    gw.transaction.Transaction.runWithNewBundle(\bundle -> {
      result.each(\elt -> {
        bundle.add(elt)
        bundle.delete(elt)
      })
    }, "su")

  }



  /**
   * Function to check Data Integrity after batch execution
   */
  @Param("createFile", int)
  @Param("insertRecordsDatassur", int)
  @Param("deleteRacordsStaging", int)
  @Returns("flag depending on success or failure")
  private function checkDataIntegrity(createFile : int , insertRecordsDatassur : int , deleteRacordsStaging : int) : int  {
    var flag = 0
    if((createFile==1) && (insertRecordsDatassur==1) && (deleteRacordsStaging==1))
    {
      flag=1
    }
    else
    {
      rollBack()
      flag=0
    }
    return flag
  }

  /**
   * Function to rollback first two steps after batch execution
   */
  private function rollBack() {
    deleteFile()
    cleanTable()
  }

}