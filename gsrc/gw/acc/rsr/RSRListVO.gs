package gw.acc.rsr

uses java.util.Date
uses java.lang.Integer

/**
 * Created with IntelliJ IDEA.
 * User: Prabhleen.Kaur
 * Date: 6/7/16
 * Time: 4:08 PM
 * POJO class to hold values to display in screen
 */
class RSRListVO {

  /*First Name of Person Contact */
  private var _firstName : String as FirstName

  /*Last Name of Person Contact */
  private var _lastName : String as LastName

  /*Name of Company Contact */
  private var _name : String as Name

  /*Date of Birth of Person Contact */
  private var _dateOfBirth : Date as DateOfBirth

  /*Vat Id of Company Contact */
  private var _vatId :String as VatId

  /*Reason of being Invalid */
  private var _reason:String as Reason

  /*1 for Person and 2 for Company */
  private var  _entityType:Integer as EntityType

  /*Individual or Legal Entity*/
  private var _entityTypeName:String as EntityTypeName

  /*Address */
  private var _primaryAddressDisplayValue:String as PrimaryAddressDisplayValue     // shows concatenated address fields

   /*Date of Event */
  private var _eventDate : String as EventDate

  /* Date of Registration*/
  private var _registrationDate : String as RegistrationDate

  /* Date of Birth to Display in Popup*/
  private var _dobString :String as DobString

}