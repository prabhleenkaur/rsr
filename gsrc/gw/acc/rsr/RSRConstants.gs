package gw.acc.rsr

uses org.slf4j.Logger
    //uses org.slf4j.LoggerFactory
uses java.util.HashMap
uses gw.pl.logging.LoggerCategory
uses gw.api.system.logging.LoggerFactory
uses com.sun.org.apache.xpath.internal.operations.String

/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 3/11/15
 * Time: 1:57 PM
 * To change this template use File | Settings | File Templates.
 */

class RSRConstants {

  public static final var _log: Logger = LoggerFactory.getLogger(LoggerCategory.INTEGRATION,"RSRPOLICY" )
  public static final var _methodIN: String = ">> IN"
  public static final var _methodOUT: String = "<< OUT"
  public static final var rsrPrefix : String = "rsr:"
  public static final var CON_Separator : String = "-:::-"
  public static final var DENIAL_PERIOD : String = "sendPolicyData"
  public static final var sendRSR : String = "sendRSR"
  public static final var sendRSRUpdate : String = "sendRSRUpdate"  //Added for sendRSRUpdate
  public static final var insBranchIM :String = "95"
  public static final var insBranchWC :String = "61"
  public static final var insBranchPA :String = "51"
  public static final var insBranchGL :String = "41"
  public static final var FSMACode     : String = "99999999"
  public static final var disputeRegis : String = "2"
  public static final var reasonCode   : String = "12"
  public static final var company   : String = "Company"
  public static final var policyHolder   : String = "PolicyHolder"
  public static final var insured   : String = "Insured"
  public static final var cancellation   : String = "Cancellation"
  public static final var nonPayment   : String = "Non-Payment"
  public static final var rsrVersion : String = "3"
  public static final var rsrRegisCode : String = "1"
  public static final var roles : HashMap<String, String> = new HashMap<String, String>(){
      "Prospect" -> "1",
      "PolicyHolder" -> "2",
      "Insured" -> "3",
      "Beneficiary" -> "4",    //TODO to impl
      "Driver" -> "6"
  }

  public static final var INS_Branch : String = "car:civilliability:fire"
  public static final var REPORT_FILE_NAME : String = "rsr_"
  public static final var REPORT_FILE_DATE_FORMAT : String = "yyyyMMddHHmm"
  public static final var REPORT_FILE_EXT : String = ".txt"
  public  static final var RSR_LIST_FILE_RSR : String = "RSR"
  public static final var RSR_LIST_FILE_LENGTH : int = 12
  public static final var LEGAL_ENTITY :String = "Legal entity"
  public static final var INDIVIDUAL :String = "Individual"
  public static final var SUBBRANCH_CODE_SEPARATOR :String =":"
  public static final var SUBBRANCH_SEPARATOR :String =","
  public static final var orgTypeIndividual :String = "1"
  public static final var orgTypeLLC :String = "4"
  public static final var orgTypeGovernment :String = "8"
  public static final var orgTypeJointVenture :String = "17"
  public static final var orgTypeOther :String = "99"
  public static final var productLineBO :String = "BusinessOwners"
  public static final var productLineIM :String = "InlandMarine"
  public static final var productLineCPP :String = "CommercialProperty"
  public static final var productLineCPK :String = "CommercialPackage"
  public static final var productLineWC :String = "WorkersComp"
  public static final var productLinePA :String = "PersonalAuto"
  public static final var productLineBA :String = "BusinessAuto"
  public static final var productLineGL :String = "GeneralLiability"
  public static final var diffDate : int = -30
  public static final var rsrAmount : int = 150
}