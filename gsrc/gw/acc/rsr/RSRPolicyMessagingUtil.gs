package gw.acc.rsr

uses java.util.HashMap
uses gw.acc.rsr.RSRData.RSRField
uses java.util.Map
uses javax.xml.stream.XMLStreamWriter
uses java.beans.XMLDecoder;
uses java.beans.XMLEncoder;
uses java.io.ByteArrayInputStream;
uses java.io.ByteArrayOutputStream;
uses gw.api.util.DisplayableException
uses java.io.FileWriter
uses java.io.BufferedWriter
uses java.lang.Exception

/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 30/10/15
 * Time: 11:22 AM
 * To change this template use File | Settings | File Templates.
 */
class RSRPolicyMessagingUtil {

  final var CLASS_NAME = "RSRPolicyMessagingUtil"

  /**
   * Function to set construct the payload
   */
  public function buildMessage(messageContext: MessageContext) {
    var METHOD_NAME = CLASS_NAME +"::: buildMessage"
    if(RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    try{
      var pp = messageContext.Root as PolicyPeriod
      if(messageContext.EventName == gw.plugin.messaging.BillingMessageTransport.CANCELPERIOD_MSG) {
        var msg = new gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod(pp)
        messageContext.createMessage(msg.asUTFString())
      } else if(messageContext.EventName == gw.plugin.messaging.BillingMessageTransport.CHANGEPERIOD_MSG) {
        var msg = new gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod(pp)
        messageContext.createMessage(msg.asUTFString())
      } else if (messageContext.EventName == "sendPolicyData"){
        var msg = new gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod(pp)
        messageContext.createMessage(msg.asUTFString())
      }
    }
        catch(ex : Exception)
        {
          RSRConstants._log.error("Unable to Create the Payload for RSR :: Event Name -- ${messageContext.EventName} Not Recognized")
          throw new DisplayableException("Unable to Create the Payload for RSR")
        }
    if(RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }

  }

  /**
   * Function to write the data in RSR Flat file
   */
  @Param("records", List<RSRData>)
  @Returns("flag depending on success or failure")
  public static function writeData(records : List<RSRData>) : int {

    var METHOD_NAME =  " :: writeData ::"
    var flag = 0
    if(RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    try{
      var writer = new FileWriter(RSRData.FilePath)
      var buffWriter = new BufferedWriter(writer)

      if(RSRConstants._log. isDebugEnabled()) {
        RSRConstants._log.debug(METHOD_NAME + "::: Number of Records to be written in the Flat File :::  ${records.Count}")
      }

      for (record in records) {
        writer.write(record.toRow()  + '\n')
      }
      writer.close()
      flag = 1
    }
        catch(ex:Exception)
        {
          flag=0
        }
    if(RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return flag
  }

}