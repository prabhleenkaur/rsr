package gw.acc.rsr

uses gw.plugin.messaging.MessageTransport
uses gw.acc.rsr.rsrdb.RSRDBBase
uses org.slf4j.Logger


/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 28/10/15
 * Time: 5:32 PM
 * To change this template use File | Settings | File Templates.
 */
class RSRMessageTransportPlugin implements MessageTransport {
  public static final var DESTINATION_ID:int = 70
  public static final var _log : Logger = RSRConstants._log
  override function send(message: Message, payload: String) {
    var METHOD_NAME = "RSRMessageTransportPlugin ::: send"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }

    var pp = message.MessageRoot as PolicyPeriod

    if(_log. isDebugEnabled()) {
      _log.debug(METHOD_NAME + " :: policyPeriod :: ${pp.PolicyNumber} -- ${pp.PublicID} ")
    }
    var stagingDB = new RSRDBBase ()
    switch(message.EventName){
      case gw.plugin.messaging.BillingMessageTransport.CANCELPERIOD_MSG:
          var xmlPayload = gw.acc.rsr.data.policyperiodcancellationmodel.PolicyPeriod.parse(payload)
          if(_log. isDebugEnabled()) {
            _log.debug(METHOD_NAME + " -- payload -- " +xmlPayload.asUTFString())
          }
          stagingDB.insertToRSR(pp,xmlPayload)
          break
      case gw.plugin.messaging.BillingMessageTransport.CHANGEPERIOD_MSG :
          var xmlPayload = gw.acc.rsr.data.policyperiodpolicychangemodel.PolicyPeriod.parse(payload)
          if(_log. isDebugEnabled()) {
            _log.debug(METHOD_NAME + " -- payload -- " +xmlPayload.asUTFString())
          }
          stagingDB.insertToRSR(pp,xmlPayload)
          break
      case RSRConstants.DENIAL_PERIOD :
          var xmlPayload = gw.acc.rsr.data.policyperiodsubmissionmodel.PolicyPeriod.parse(payload)
          if(_log. isDebugEnabled()) {
            _log.debug(METHOD_NAME + " -- payload -- " +xmlPayload.asUTFString())
          }
          stagingDB.insertToRSR(pp,xmlPayload)
          break
    }

    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    message.reportAck()
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  override function setDestinationID(p0: int) {
  }

}