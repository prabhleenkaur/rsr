package gw.acc.rsr

uses gw.api.database.Query
uses java.util.ArrayList
uses org.apache.http.util.EncodingUtils
uses gw.api.system.database.SequenceUtil
uses java.util.Date
uses gw.api.util.DisplayableException
uses gw.util.GosuStringUtil
uses com.google.common.base.Strings
uses gw.acc.rsr.rsrdb.RSRDataMapper
uses java.text.SimpleDateFormat
uses org.apache.commons.lang.time.DateUtils
uses org.apache.commons.codec.binary.StringUtils
/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 30/10/15
 * Time: 10:10 AM
 * RSR Helper class
 */
class RSRPolicyHelper {
  var _log = RSRConstants._log
  static final var CLASS_NAME = "RSRPolicyHelper :::"
  /**
   * Function to validate if the PolicyPeriod needs to be reported to RSR
   */
  public function rsrValueCheck(policyPeriod: PolicyPeriod, bean: Bean): List<RSRValueCheck_Ext> {
    var METHOD_NAME = CLASS_NAME + "rsrValueCheck"

    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var val: Boolean = null
    var rsrCode: List<RSRValueCheck_Ext> = new List<RSRValueCheck_Ext>()
    // policyPeriod Will be used by the eval object
    var cancellation: Cancellation = null
    var policyChange: PolicyChange = null
    var submission: Submission = null
    if (bean typeis Cancellation) {
      cancellation = bean
      // Used by the eval object
    } else if (bean typeis PolicyChange) {
      policyChange = bean
    } else if (bean typeis Submission) {
      submission = bean
    }

    Query.make(RSRValueCheck_Ext).select().toList().each(\con -> {
      var rules = "${con.RsrRules}"
      if (_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + " :: RSR Entry Check Rule ::" + rules)
      }
      val = eval(con.RsrRules) as Boolean
      if (_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + " :: RSR Entry Check Rule Result :: " + val)
      }
      if (val) {
        rsrCode.add(con)
      }
    })
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return rsrCode
  }

  /**
   * Returns Fixed Witdh UTF-8 Value
   */
  @Param("s", "input string")
  @Param("len", "Field width")
  @Returns("Fixed width UTF-8 String")
  static function getFixedWitdhUTF8Value(s: String, len: int): String {
    var str = StringUtils.getBytesUtf8(s)
    s = new String(str, "UTF-8")
    if (s.length == len) {
      return s
    }

    if (s.length < len) {
      return s + getSpaces(len - s.length)
    }

    return s.substring(0, len)
  }

  /**
   * To get the space
   */
  static function getSpaces(n: int): String {
    return new String(new char[n]).replaceAll('\0', ' ');
  }

  /**
   * To generate the RSR Number
   */
  @Returns("RSR Number")
  public static function generateRSRNumber(): String {
    var nextSeq = SequenceUtil.next(0000001, "RSR_CONSTANT") as String
    nextSeq = Strings.padStart(nextSeq, 7, "0")
    return nextSeq
  }

  /**
   * Function to initiate the RSR Report
   */
  public function initiateRSR(bean: Bean, policyPeriod: PolicyPeriod, evtDate: List<Date>, regDate: List<Date>) {
    var METHOD_NAME = CLASS_NAME + "initiateRSR"

    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var cancellation: Cancellation = null
    var policyChange: PolicyChange = null
    var submission: Submission = null
    var rsrCodes: List<RSRValueCheck_Ext>
    var rsrPolicy: RSRPolicydata_Ext = null

    rsrCodes = rsrValueCheck(policyPeriod, bean)

    if (_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + " :: Entry Count ::" + rsrCodes.Count)
    }

    if (bean typeis Cancellation) {
      cancellation = bean
    } else if ((bean typeis PolicyChange)) {
      policyChange = bean
    } else if (bean typeis Submission) {
      submission = bean
    }

    if (cancellation.CancelReasonCode == typekey.ReasonCode.TC_FRAUD or cancellation.CancelReasonCode == typekey.ReasonCode.TC_CRIMINAL or cancellation.CancelReasonCode == typekey.ReasonCode.TC_NONREPORT
        or cancellation.CancelReasonCode == typekey.ReasonCode.TC_LOSSHISTORY or cancellation.CancelReasonCode == typekey.ReasonCode.TC_PAYMENTHISTORY or cancellation.CancelReasonCode == typekey.ReasonCode.TC_INSURERLIEDDEC
        or cancellation.CancelReasonCode == typekey.ReasonCode.TC_INFONOTPROVIDED) {
      if (cancellation != null) {
        //cancellation = bean
        cancellation.RSRData_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = cancellation.RSRData_ExtID
      } else if ((policyChange != null)) {
        //policyChange = bean
        policyChange.RSRPolicydata_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = policyChange.RSRPolicydata_ExtID
      } else if (submission != null) {
        //submission = bean
        submission.RSRSubPolicydata_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = submission.RSRSubPolicydata_ExtID
      }
      // rsrPolicy.rsrNumber = generateRSRNumber()
      rsrPolicy.DisputeRegis = false
      rsrPolicy.DoubtableAddr = false
      rsrPolicy.FSMACode = "99999999"
      var eventDate = gw.api.util.DateUtil.currentDate()
      var regisDate = gw.api.util.DateUtil.currentDate()
      evtDate.add(eventDate.trimToMidnight())
      regDate.add(regisDate.trimToMidnight())
    }

    else if (rsrCodes.Count > 0 and (policyChange.RSRPolicydata_ExtID == null or
        submission.RSRSubPolicydata_ExtID == null or
        cancellation.RSRData_ExtID == null)) {
      if (cancellation != null) {
        //cancellation = bean
        cancellation.RSRData_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = cancellation.RSRData_ExtID
      } else if ((policyChange != null)) {
        //policyChange = bean
        policyChange.RSRPolicydata_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = policyChange.RSRPolicydata_ExtID
      } else if (submission != null) {
        //submission = bean
        submission.RSRSubPolicydata_ExtID = new RSRPolicydata_Ext(gw.transaction.Transaction.getCurrent())
        rsrPolicy = submission.RSRSubPolicydata_ExtID
      }
      //  rsrPolicy.rsrNumber = generateRSRNumber()
      rsrPolicy.DisputeRegis = false
      rsrPolicy.DoubtableAddr = false
      rsrPolicy.FSMACode = "99999999"

      rsrCodes.each(\code -> {
        var eventDate = eval(code.EventDate) as Date
        var regisDate = eval(code.RegistrationDate) as Date
        evtDate.add(eventDate.trimToMidnight())
        regDate.add(regisDate.trimToMidnight())

        if (bean typeis Cancellation and (bean.CancelReasonCode == ReasonCode.TC_NONPAYMENT or
            bean.CancelReasonCode == ReasonCode.TC_CANCEL)
            and policyPeriod.TotalPremiumRPT.Amount >= 150) {
          rsrPolicy.RSRAmount = policyPeriod.TotalPremiumRPT.Amount
        }
      })
    } else if (rsrCodes.Count == 0) {
      if (bean typeis Cancellation) {
        cancellation = bean
        if (cancellation.RSRData_ExtID != null) {
          cancellation.RSRData_ExtID = null
        }
      } else if (bean typeis PolicyChange) {
        policyChange = bean
        if (policyChange.RSRPolicydata_ExtID != null) {
          policyChange.RSRPolicydata_ExtID = null
        }
      } else if (bean typeis Submission) {
        submission = bean
        if (submission.RSRSubPolicydata_ExtID != null) {
          submission.RSRSubPolicydata_ExtID = null
        }
      }
    } else {
      if (_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + RSRConstants._methodOUT + "RSRPolicyData shall be available")
      }
    }


    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Function to get the concatenated Ins and Sub Branch
   * details of RSR Entry
   */
  public function getInsuranceValues(ins: RSRInsuranceBranch_Ext, sub: String): String {
    var METHOD_NAME = CLASS_NAME + "getInsuranceValues"
    var ins_str: String = null
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    if (_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "::: Ins Values ::: " + ins)
    }

    ins_str = toRefactorValues(ins, sub)

    if (_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "::: Ins Values After Refoctoring :::" + ins)
    }
    var subBranches = sub != null or !sub.Empty ? sub?.remove(":") : GosuStringUtil.EMPTY
    var retVal: String = null

    retVal = ins_str + subBranches
    if (_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "::: Return Value of INS Branch ::: " + retVal)
    }
    if (retVal.containsIgnoreCase("null")){
      retVal = retVal.remove("null")
    }
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }

    return retVal
  }

  /**
   * Function to refactor the Values with appropriate codes
   * details of RSR Entry
   */
  private function toRefactorValues(ins: RSRInsuranceBranch_Ext, sub: String): String {
    var METHOD_NAME = CLASS_NAME + "toRefactorValues"
    var retVal: String = ins.Code
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    if (sub == null and RSRConstants.INS_Branch.containsIgnoreCase(ins.Code)) {
      if (ins.Code?.containsIgnoreCase("car")) {
        retVal = "5"
      }
      if (ins.Code?.containsIgnoreCase("fire")) {
        retVal = "3"
      }
      if (ins.Code?.containsIgnoreCase("civilliability")){
        retVal = "4"
      }
    }

    if (sub != null) {

      if ((ins.Code?.containsIgnoreCase("car") or ins.Code?.containsIgnoreCase("fire") or
          ins.Code?.containsIgnoreCase("civilliability")) and sub.length == 1) {
        retVal = GosuStringUtil.EMPTY
      }

      sub?.split(":").toList()?.each(\elt -> {

        var val = RSRInsSub_Branch_Ext.get(elt)

        if (RSRConstants.INS_Branch.containsIgnoreCase(ins)){
          if (val?.hasCategory(typekey.RSRInsuranceBranch_Ext.TC_CAR)) {
            retVal = retVal?.remove("car")
          }
          if (val?.hasCategory(typekey.RSRInsuranceBranch_Ext.TC_FIRE)) {
            retVal = retVal?.remove("fire")
          }
          if (val?.hasCategory(typekey.RSRInsuranceBranch_Ext.TC_CIVILLIABILITY)) {
            retVal = retVal?.remove("civilliability")
          }
        }
      })
    }


    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return retVal
  }

  /**
   * Function to set the default values from PCF
   */
  function setValues(rsrPolicy: RSRPolicydata_Ext, policyPeriod: PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "setValues"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var rsrVal = Query.make(RSRValueCheck_Ext).compare("RsrReason", Equals, rsrPolicy.rsrCancelReason.Code)
    var Val: RSRValueCheck_Ext = null
    //  policyPeriod.LastReportedDate
    if (rsrVal.select().Count == 1) {
      Val = rsrVal.select().first()
    } else if (policyPeriod.Job typeis PolicyChange) {
      if (rsrPolicy.rsrCancelReason == RSRCancelReason_Ext.TC_INSUREDLIED) {
        Val = rsrVal.compare("RsrCode", Equals, "41").select().first()
      } else if (rsrPolicy.rsrCancelReason == RSRCancelReason_Ext.TC_MULTILEPOLICY or
          rsrPolicy.rsrCancelReason == RSRCancelReason_Ext.TC_PHFRAUD) {
        Val = rsrVal.select().first()
      }
    }
    else if (policyPeriod.Job typeis Cancellation) {
        if (rsrPolicy.rsrCancelReason == RSRCancelReason_Ext.TC_INSUREDLIED) {
          Val = rsrVal.compare("RsrCode", Equals, "41").select().first()
        }
      }
      else if (policyPeriod.Job typeis Submission) {
          if (rsrPolicy.rsrCancelReason == RSRCancelReason_Ext.TC_INSUREDLIED) {
            Val = rsrVal.compare("RsrCode", Equals, "41").select().first()
          }
          else {
            Val = rsrVal.compare("RsrCode", Equals, "32").select().first()
          }
        }
    rsrPolicy.rsrCancelReason = RSRCancelReason_Ext.get(Val.RsrReason)
    rsrPolicy.RsrReasonCode = Val.RsrCode
    if (rsrPolicy.EventDate == null) {
      var eventDate = eval(Val.EventDate) as Date
      rsrPolicy.EventDate = eventDate
    }
    if (rsrPolicy.RegisDate == null) {
      var regisDate = eval(Val.RegistrationDate) as Date
      rsrPolicy.RegisDate = regisDate
    }
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Function to get the valid RSR reasons in screen
   */
  function getRSRReasons(bean: Bean, policyPeriod: PolicyPeriod): List<RSRCancelReason_Ext> {
    var METHOD_NAME = CLASS_NAME + "getRSRReasons"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var retVal = new List<RSRCancelReason_Ext>()
    if (bean typeis Cancellation) {

      if (bean.CancelReasonCode == ReasonCode.TC_NONPAYMENT or bean.CancelReasonCode == ReasonCode.TC_CANCEL) {
        // retVal.add(RSRCancelReason_Ext.TC_NA)
        retVal.add(RSRCancelReason_Ext.TC_DNREFUND)
      } else if (policyPeriod.checkReason_16(bean)) {
        retVal.add(RSRCancelReason_Ext.TC_REFUSETOPAY)
      } else if (bean.CancelReasonCode == ReasonCode.TC_UWREASONS or policyPeriod.checkReason_17(bean)) {
        retVal.add(RSRCancelReason_Ext.TC_LOSSHISTORY)
        retVal.add(RSRCancelReason_Ext.TC_CLAIMSREFUND)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_LOSSHISTORY or bean.CancelReasonCode == ReasonCode.TC_RISKCHANGE or bean.CancelReasonCode == ReasonCode.TC_VIOLATION
          or bean.CancelReasonCode == ReasonCode.TC_CRIMINAL or bean.CancelReasonCode == ReasonCode.TC_CONDEMN or bean.CancelReasonCode == ReasonCode.TC_PAYMENTHISTORY){
        retVal.add(RSRCancelReason_Ext.TC_EXCESSIVECLAIM)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_FRAUD or bean.CancelReasonCode == ReasonCode.TC_NONREPORT or bean.CancelReasonCode == ReasonCode.TC_FAILCOOP
          or bean.CancelReasonCode == ReasonCode.TC_INFONOTPROVIDED){
        retVal.add(RSRCancelReason_Ext.TC_CONTRARYLOSS)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_FRAUD or bean.CancelReasonCode == ReasonCode.TC_CRIMINAL or bean.CancelReasonCode == ReasonCode.TC_LOSSHISTORY){
        retVal.add(RSRCancelReason_Ext.TC_INTENTIONALDAMAGE)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_FRAUD){
        retVal.add(RSRCancelReason_Ext.TC_POLICYAGREEMENT)
        retVal.add(RSRCancelReason_Ext.TC_PHFRAUD)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_LOSSHISTORY){
        retVal.add(RSRCancelReason_Ext.TC_LOSSHISTORY)
      }
      if (bean.CancelReasonCode == ReasonCode.TC_INSURERLIEDDEC){
        retVal.add(RSRCancelReason_Ext.TC_INSUREDLIED)
      }
    } else if (bean typeis PolicyChange) {
      retVal.add(RSRCancelReason_Ext.TC_INSUREDLIED)
      retVal.add(RSRCancelReason_Ext.TC_MULTILEPOLICY)
      retVal.add(RSRCancelReason_Ext.TC_PHFRAUD)
      retVal.add(RSRCancelReason_Ext.TC_EXCESSIVECLAIM)
      retVal.add(RSRCancelReason_Ext.TC_INTENTIONALDAMAGE)
      retVal.add(RSRCancelReason_Ext.TC_CONTRARYLOSS)
      retVal.add(RSRCancelReason_Ext.TC_POLICYAGREEMENT)
    } else if (bean typeis Submission) {
      if (bean.RejectReason == ReasonCode.TC_PROSPECTLIED) {
        retVal.add(RSRCancelReason_Ext.TC_PROSPECTLIED)
      }
      if (bean.RejectReason == ReasonCode.TC_INFONOTPROVIDED or bean.RejectReason == ReasonCode.TC_PRODREQUIREMENTS) {
        retVal.add(RSRCancelReason_Ext.TC_CONTRARYINFO)
      }
    }
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return retVal
  }

  /**
   * Function to nullify the unwanted components
   */
  public static function setBeforeCommit(bean: Bean) {
    var METHOD_NAME = CLASS_NAME + "setBeforeCommit"
    if (RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }

    if (bean typeis Cancellation) {
      if (bean.RSRData_ExtID.RSRInsBranch == null) {
        bean.RSRData_ExtID = null
      }
    } else if (bean typeis PolicyChange) {
      if (bean.RSRPolicydata_ExtID.RSRInsBranch == null and bean.RSRPolicydata_ExtID.eventDate == null) {
        bean.RSRPolicydata_ExtID = null
      }
    } else if (bean typeis Submission) {
      if (bean.RSRSubPolicydata_ExtID.RSRInsBranch == null and bean.RSRSubPolicydata_ExtID.eventDate == null) {
        bean.RSRSubPolicydata_ExtID = null
      }
    }

    if (RSRConstants._log.isTraceEnabled()) {
      RSRConstants._log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Function to get the PolicyChange Values for the User to
   * override if needed
   */
  function getChangedValues(pp: PolicyPeriod, policyChange: PolicyChange): Boolean {
    var METHOD_NAME = CLASS_NAME + "getChangedValues"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    if (policyChange.GenerateRSR_Ext == true){
      var latestPC = pp.Policy.Jobs.where(\elt -> elt typeis PolicyChange and elt.ResultingBoundPeriod.PolicyNumber == pp.PolicyNumber and
          elt.GenerateRSR_Ext == true).orderByDescending(\p -> p.CreateTime).first() as PolicyChange
      if (latestPC != null) {
        policyChange.RSRPolicydata_ExtID = latestPC.RSRPolicydata_ExtID

        if (_log.isTraceEnabled()) {
          _log.trace(METHOD_NAME + RSRConstants._methodOUT)
        }
        return true
      }
    }
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }


    return false
  }

  /**
   * Function to check if the RSR Report has been sent to the Contact Already
   */
  private function qualifyRSR(rsrPolicy: RSRPolicydata_Ext): Boolean {
    var qry = Query.make(RSRPolicydata_Ext)
    var conIDs = rsrPolicy.ContactID
    if (conIDs.Empty == false and conIDs != null) {
      var validateCon = qry.contains("ContactID", conIDs, true)
      if (validateCon.select().Count > 0){
        var match = validateCon.select().toList()
        for (one in match) {
          if (one.RSRInsBranch == rsrPolicy.RSRInsBranch) {
            if (one.rsrCancelReason == rsrPolicy.rsrCancelReason) {
              return false
            }
          }
        }
      }
    }

    if (rsrPolicy.ComDriverID != null and rsrPolicy.ComDriverID.Empty == false) {
      var validateDriver = qry.contains("ComDriverID", rsrPolicy.ComDriverID, true)
      if (validateDriver.select().Count > 0) {
        var match = validateDriver.select().toList()
        for (one in match) {
          if (one.RSRInsBranch == rsrPolicy.RSRInsBranch) {
            if (one.rsrCancelReason == rsrPolicy.rsrCancelReason) {
              return false
            }
          }
        }
      }
    }
    return true
  }

  /**
   * Function to check if the RSR Report has been sent to the Contact Already
   */
  function validateRSR(rsrPolicy: RSRPolicydata_Ext, policyPeriod: PolicyPeriod) {
    var METHOD_NAME = CLASS_NAME + "validateRSR"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var isExist: Boolean = false

    if (rsrPolicy.ContactID == null and rsrPolicy.ComDriverID == null) {
      throw new DisplayableException("Please select the Contact to send the RSR Report")
    }

    if (rsrPolicy.ComDriverID != null and (rsrPolicy.RsrReasonCode == "12" or
        rsrPolicy.RsrReasonCode == "31" or rsrPolicy.RsrReasonCode == "32" or rsrPolicy.RsrReasonCode == "41" or rsrPolicy.RsrReasonCode == "43")){
      throw new DisplayableException("Please select the different contact for this RSR Reason. Driver Contacts are not allowed")
    }

    if (rsrPolicy.ContactID != null) {
      var conIDs = rsrPolicy.ContactID.split(RSRConstants.CON_Separator)
      var isDriver: Boolean = false
      conIDs?.each(\con -> {
        var contact = Contact(con) as Contact
        var accCons = contact.AccountContacts
        var roleMatch1 = policyPeriod.PolicyContactRoles.hasMatch(\p -> p.ContactDenorm.ID == contact.ID and (typeof p).TypeInfo.DisplayName.containsIgnoreCase("insured") == true and
            (typeof p).TypeInfo.DisplayName.containsIgnoreCase("primary") == false)
        var roleMatchPrimary = policyPeriod.PolicyContactRoles.hasMatch(\p -> p.ContactDenorm.ID == contact.ID and (typeof p).TypeInfo.DisplayName.containsIgnoreCase("primary") == true)
        var roleMatch2 = policyPeriod.PolicyContactRoles.hasMatch(\elt1 -> (elt1 typeis PolicyDriver) and elt1.ContactDenorm.ID == contact.ID)
        if ((rsrPolicy.RsrReasonCode == "12" or
            rsrPolicy.RsrReasonCode == "31" or rsrPolicy.RsrReasonCode == "32" or
            rsrPolicy.RsrReasonCode == "41" or rsrPolicy.RsrReasonCode == "43")) {
          if ((roleMatch2 and !roleMatchPrimary) or roleMatch1)

            throw new DisplayableException("Please select the different contact for this RSR Reason. Only Primary Insured's are allowed")
        }
      })
    }

    if (rsrPolicy != null) {
      isExist = qualifyRSR(rsrPolicy)
    }

    if (_log.isDebugEnabled()) {
      _log.debug(METHOD_NAME + "::: Is the RSR Report for Contact Already Generated ::" + isExist)
    }
    if (isExist == false) {
      rsrPolicy = null
      if (_log.isDebugEnabled()) {
        _log.debug(METHOD_NAME + "::: Nullified RSRPolicy ::" + rsrPolicy)
      }
      throw new DisplayableException("The Contact is already Reported to RSR. Please cancel the transaction.")
    }

    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Function to check the existence of RSR for that period
   */
  function isRSRExist(pp: PolicyPeriod): Boolean {
    var METHOD_NAME = ":::: isRSRExist ::::"
    if (RSRConstants._log.isDebugEnabled()) {
      RSRConstants._log.debug(METHOD_NAME + pp.PublicID)
    }
    var qry = Query.make(RSRPolicydata_Ext)
    var conIDs = pp.getPolicyContactsID().split(RSRConstants.CON_Separator)
    if (RSRConstants._log.isDebugEnabled()) {
      RSRConstants._log.debug(METHOD_NAME + "contacts IDs" + conIDs)
    }
    qry.compareIn("ContactID", conIDs)
    var rsrPolicyRecords = qry.select()
    var validateFlag: Boolean = false
    if (rsrPolicyRecords.Count > 0){
      validateFlag = true
    }
    if (RSRConstants._log.isDebugEnabled()) {
      RSRConstants._log.debug(METHOD_NAME + "Validate :: Ret" + validateFlag)
    }
    return validateFlag
  }

  /*
   Function to check whether policy Contacts are listed in RSR List or not.
   If yes then these are added in the list of Invalid contacts.
   */

  @Param("policyPeriod", PolicyPeriod)
  function checkContactsInRSRList(policyPeriod: PolicyPeriod): ArrayList<RSRListVO> {
    var METHOD_NAME = CLASS_NAME + "checkContactsInRSRList"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }

    var rsrDataMapper = new RSRDataMapper()
    var RSRListContacts = rsrDataMapper.getRSRInvalidContacts()
    var RSRInvalidContacts = new ArrayList<RSRListVO>()
    var accountContacts = policyPeriod.getAllAccountContacts()
    var vatNumber: String = GosuStringUtil.EMPTY

    var count = 0
    for (contact in accountContacts) {
      if (contact.Person){
        RSRListContacts.each(\rsrListContact -> {
          if (rsrListContact.EntityType == 1){
            // 1 is for Person
            var personContact = contact.Contact as Person
            if (rsrListContact.DateOfBirth != null and rsrListContact.DateOfBirth != "" and rsrListContact.FirstName != "" and rsrListContact.FirstName != null) {
              if (personContact.LastName.equalsIgnoreCase(rsrListContact.LastName) and personContact.FirstName.equalsIgnoreCase(rsrListContact.FirstName) and DateUtils.isSameDay(personContact.DateOfBirth, rsrListContact.DateOfBirth)){
                count++
                RSRInvalidContacts.add(rsrListContact)
                _log.debug(METHOD_NAME + "Neither FirstName nor Date of Birth is null" + "::: Person added to RSR Invalid List ::")
              }
            }
            else if (rsrListContact.FirstName == "" or rsrListContact.FirstName.Empty) {
              if (personContact.LastName.equalsIgnoreCase(rsrListContact.LastName) and DateUtils.isSameDay(personContact.DateOfBirth, rsrListContact.DateOfBirth)){
                count++
                RSRInvalidContacts.add(rsrListContact)
                _log.debug(METHOD_NAME + "First name is null" + "::: Person added to RSR Invalid List ::")
              }
            }
            else {
              if (personContact.LastName.equalsIgnoreCase(rsrListContact.LastName) and personContact.FirstName.equalsIgnoreCase(rsrListContact.FirstName)){
                count++
                RSRInvalidContacts.add(rsrListContact)
                _log.debug(METHOD_NAME + "Date of Birth is null" + "::: Person added to RSR Invalid List ::")
              }
            }
          }
        }
        )
      }
      if (contact.Company){
        RSRListContacts.each(\rsrListContact -> {
          if (rsrListContact.EntityType == 2){
            // 2 is for Company
            var companyContact = contact.Contact as Company
            if (companyContact.VATNum_Ext != null and companyContact.VATNum_Ext != "" and rsrListContact.VatId != null and rsrListContact.VatId != "") {
              if (companyContact.VATNum_Ext?.containsIgnoreCase("BE0")){
                //Belgian VAT number starts from BE0
                vatNumber = companyContact.VATNum_Ext?.split("BE0").toList().get(1)
              }
              if (companyContact.Name.equalsIgnoreCase(rsrListContact.Name) and vatNumber.equalsIgnoreCase(rsrListContact.VatId)){
                count++
                RSRInvalidContacts.add(rsrListContact)
              }
            }
            else {
              if (companyContact.Name.equalsIgnoreCase(rsrListContact.Name)){
                count++
                RSRInvalidContacts.add(rsrListContact)
                _log.debug(METHOD_NAME + "::: Company added to RSR Invalid List ::")
              }
            }
          }
        }
        )
      }
    }

    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
    return RSRInvalidContacts
  }

  /*
   Function called to check whether to display link and label or not
   */

  @Returns("True or False depending on count of Invalid contacts")
  @Param("policyPeriod", PolicyPeriod)
  function checkCountInRSR(policyPeriod: PolicyPeriod): Boolean {
    var METHOD_NAME = CLASS_NAME + "checkCountInRSR"
    if (_log.isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    var RSRListContacts = new ArrayList<RSRListVO>()
    RSRListContacts = checkContactsInRSRList(policyPeriod)
    if (RSRListContacts.Count > 0) {
      return true
    }
    else
      return false
  }

  @Returns("Date in a given format")
  @Param("date", Date)
  private static function convertToDateFormat(date: Date): Date {
    var dateFormat: SimpleDateFormat = new SimpleDateFormat("dd-MM-yyyy")
    var retDate = dateFormat.format(date)
    return retDate
  }

  /*
   Function to return label for amount field depending on reason selected
   */

  @Returns("Label to display on RSR Screen")
  @Param("rsrReasonCode", String)
  function getAmountLabel(rsrReasonCode: String): String {
    var label: String = GosuStringUtil.EMPTY
    if (rsrReasonCode == "12") {
      label = displaykey.Web.Job.Policy.RSRAmount
    }
    else if (rsrReasonCode == "18" or rsrReasonCode == "23"){
      label = displaykey.Unpaid.Amount
    }
    return label
  }
}