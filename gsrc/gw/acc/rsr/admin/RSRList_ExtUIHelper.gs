package gw.acc.rsr.admin

uses gw.acc.rsr.rsrdb.RSRDBBase
uses java.util.HashMap
uses gw.api.database.Query
uses java.util.ArrayList
uses gw.api.database.Relop
uses gw.acc.rsr.RSRConstants
uses org.slf4j.Logger
uses gw.api.util.DisplayableException

/**
 * Created with IntelliJ IDEA.
 * User: vpennington
 */
class RSRList_ExtUIHelper {

  public static final var _log : Logger = RSRConstants._log
  var _searchType     : String as SearchType
  var _searchCriteria : RSRList_ExtSearchCriteria

  construct() {

  }

  construct( searchCriteria : RSRList_ExtSearchCriteria ){

    _searchCriteria = searchCriteria
  }

 /**
  *
  **/
  function toggleSearchType( currentValue : String ) : String {

    if ( currentValue == "Company" ) {
      _searchCriteria.DateOfBirth = null
      _searchCriteria.FirstName = null

      return "Person"

    } else if ( currentValue == "Person" ) {
      _searchCriteria.VATNumber = null

      return "Company"

    } else {

      return currentValue
    }
  }

 /**
  *
  **/
  function getDisplayAddressStr( rsrList : RSRList_Ext ) : String {

    var returnVal : String = null

    var houseNumber = ( rsrList.HouseNumber != null ) ? "${rsrList.HouseNumber} " : ""
    var street = ( rsrList.Street != null ) ? "${rsrList.Street} " : ""
    var boxNumber = ( rsrList.BoxNumber != null ) ? ", ${rsrList.BoxNumber}," : ""
    var city = ( rsrList.City != null ) ? "${rsrList.City} " : ""
    var zipCode = ( rsrList.ZipCode != null ) ? "${rsrList.ZipCode} " : ""
    returnVal = "${houseNumber}${street}${boxNumber}${city}${zipCode}"

    return returnVal
  }

  function getDisplayAddressStr( rsrListUpdate : RSRListUpdate_Ext ) : String {

    var returnVal : String = null

    var houseNumber = ( rsrListUpdate.HouseNumber != null ) ? "${rsrListUpdate.HouseNumber} " : ""
    var street = ( rsrListUpdate.Street != null ) ? "${rsrListUpdate.Street} " : ""
    var boxNumber = ( rsrListUpdate.BoxNumber != null ) ? ", ${rsrListUpdate.BoxNumber}," : ""
    var city = ( rsrListUpdate.City != null ) ? "${rsrListUpdate.City} " : ""
    var zipCode = ( rsrListUpdate.ZipCode != null ) ? "${rsrListUpdate.ZipCode} " : ""
    returnVal = "${houseNumber}${street}${boxNumber}${city}${zipCode}"

    return returnVal
  }

 /**
  * Set up a direct connection to the staging database and then make an insert of the values in the POGO into the
  * staging table.
  **/
//  function sendRecordToStagingTable( rsrList : RSRListUpdate_Ext ) {
//
//    var integrationDB = new RSRDBBase()
//
//    integrationDB.insertUpdateToRSRStaging( rsrList )
//  }


  public function getReasonCodesMap() : HashMap< String, String > {

    var codeReasonsMap = new HashMap< String, String >()

    var query = Query.make( RSRValueCheck_Ext )
    var results = query.select()
    var checkValues = results.toTypedArray()
    var checkValuesByCode = checkValues.partition( \ cv -> cv.rsrCode )
    var codes = checkValuesByCode.Keys.toList()
    checkValuesByCode.eachKeyAndValue( \ code, reasons -> {
      var reasonStr = ""
      var reasonCode : String = null
      if ( reasons.Count == 0 ) {
      } else if ( reasons.Count > 1 ) {
        reasons.each( \ re -> {
          var separator = ( reasonStr == "" ) ? "" : " or "
          var reasonName = typekey.RSRCancelReason_Ext.get( re.rsrReason ).DisplayName
          reasonStr = reasonStr + separator + typekey.RSRCancelReason_Ext.get( re.rsrReason ).DisplayName
        } )
      } else {
        reasonCode = reasons.single().rsrReason
        reasonStr = typekey.RSRCancelReason_Ext.get( reasonCode ).DisplayName
      }
      codeReasonsMap.put( code, reasonStr )
    } )

    return codeReasonsMap
  }


  public function getReasonCodeList() : List< String > {

    var reasonCodesMap = getReasonCodesMap()
    var reasonCodeList = new ArrayList< String >()

    reasonCodesMap.eachKeyAndValue( \ code, reason -> {
      reasonCodeList.add( "${code} - ${reason}" )
    } )

    return reasonCodeList
  }

///**
//  * Using the RSR Number as the key query for the corresponding record in te Records_sent_Datassur_Ext table.
//  * If corresponding records are found form a hashset based on the policy number field. If there is only one policy
//  * number return that. if there is more then one policy number return an appropriate string. Log if there is more than
//  * one matching policy number
//  **/
//  function getRelatedPolicyNo( rsrList : RSRList_Ext ) : String {
//
//    var rsrNumber = rsrList.RSRNumber
//    var query = Query.make( Records_sent_Datassur_Ext ).compare( "RSRNumber", Relop.Equals, rsrNumber )
//    var results = query.select()
//    var msgNotFound = displaykey.Web.Admin.RSR_Ext.Details.PolicyNumber.NotFound.Value
//
//    if ( results.Count > 1 ) {
//      var hSet = new java.util.HashSet< String >()
//      results.each( \ r -> {
//        hSet.add( r.PolicyNumber )
//      } )
//      if ( hSet.Count == 1 ) {
//
//        return results.first().PolicyNumber
//      } else {
//        if (_log.isTraceEnabled() ) {
//          _log.trace( "A unique policy number was not found for RSR Number: ${rsrNumber}")
//        }
//
//        return msgNotFound
//      }
//    } else if ( results.Count == 0 ) {
//
//      return msgNotFound
//    } else {
//
//      return results.first().PolicyNumber
//    }
//  }

 /**
  * Create a new RSR Update record and set the values from the RSR List record.
  **/
  function initializeRSRListUpdate( rsrList : RSRList_Ext ) : RSRListUpdate_Ext {

    var rsrListUpdate = new RSRListUpdate_Ext()
    rsrListUpdate = resetRSRListUpdateValues( rsrList, rsrListUpdate )

    return rsrListUpdate
  }

 /**
  * Set the values on the RSR Update record from the RSR List record. Used on intitial creation and subsequently when
  **/
  function resetRSRListUpdateValues( rsrList : RSRList_Ext, rsrListUpdate : RSRListUpdate_Ext ) : RSRListUpdate_Ext {

    rsrListUpdate.Name                          = rsrList.Name
    rsrListUpdate.Street                        = rsrList.Street
    rsrListUpdate.HouseNumber                   = rsrList.HouseNumber
    rsrListUpdate.BoxNumber                     = rsrList.BoxNumber
    rsrListUpdate.ZipCode                       = rsrList.ZipCode
    rsrListUpdate.City                          = rsrList.City
    rsrListUpdate.LanguageCode                  = typekey.RSRLanguageCode_Ext.get( rsrList.LanguageCode )
    rsrListUpdate.PersonCompany                 = typekey.RSRPersonCompany_Ext.get( rsrList.PersonCompany )
    rsrListUpdate.FirstName                     = rsrList.FirstName
    rsrListUpdate.DateOfBirth                   = rsrList.DateOfBirth
    rsrListUpdate.Gender                        = typekey.RSRGender_Ext.get( rsrList.Gender )
    rsrListUpdate.JuridicalForm                 = typekey.RSRJuridicalForm_Ext.get( rsrList.Judicialform )
    rsrListUpdate.VATNumber                     = rsrList.VATNumber
    rsrListUpdate.RiskStreet                    = rsrList.RiskStreet
    rsrListUpdate.RiskHouseNumber               = rsrList.RiskHouseNumber
    rsrListUpdate.RiskBoxNumber                 = rsrList.RiskBoxNumber
    rsrListUpdate.RiskZipCode                   = rsrList.RiskZipCode
    rsrListUpdate.RiskCity                      = rsrList.RiskCity
    rsrListUpdate.Role                          = typekey.RSRRole_Ext.get( rsrList.Role )
    rsrListUpdate.InsuranceBranch               = rsrList.InsuranceBranch
    rsrListUpdate.EventDate                     = rsrList.EventDate
    rsrListUpdate.EventIdentificationDate       = rsrList.EventIdentificationDate

    rsrListUpdate.Amount                        = rsrList.Amount
    rsrListUpdate.CodeReason                    = setCodeReasonDisplayString( rsrList.CodeReason )
    rsrListUpdate.Dispute                       = typekey.RSRYesNoAsDefault_Ext.get( rsrList.Dispute )
    rsrListUpdate.DoubtableAddress              = typekey.RSRYesNoAsDefault_Ext.get( rsrList.DoubtableAddress )
    rsrListUpdate.FicheRSR                      = rsrList.FicheRSR
    rsrListUpdate.NumberClaims_RegisteredFault  = rsrList.NumberClaims_RegisteredFault
    rsrListUpdate.NumberClaims_RegisteredPerson = rsrList.NumberClaims_RegisteredPerson
    rsrListUpdate.RegistrationCode              = typekey.RSRRegistrationCode_Ext.get( rsrList.RegistrationCode )
    rsrListUpdate.ReleaseCode                   = rsrList.ReleaseCode
    rsrListUpdate.RSRDossierNumber              = rsrList.RSRDossierNumber
    rsrListUpdate.RSRNumber                     = rsrList.RSRNumber
    rsrListUpdate.RSRSheetDate                  = rsrList.RSRSheetDate
    rsrListUpdate.PolicyNumber                  = rsrList.RelatedPolicyNo//getRelatedPolicyNo( rsrList )

    return rsrListUpdate
  }

 /**
  * Get the list of code - reason strings that are displayed to the user. Match the original code value against the
  * code part of the code - reason string, so that we can start with the right value.
  **/
  private function setCodeReasonDisplayString( codeValue : String ) : String {

    var crList = getReasonCodeList()
    var codeReasonStr = crList.firstWhere( \ cr -> codeValue == cr.substring( 0, ( cr.indexOf( "-" ) - 1 ) ).trim() )

    return codeReasonStr
  }

 /**
  * Used on the amount field to trim the leading zeroes to make it easier to edit the value.
  **/
  function trimLeadingZeroes( value : String ) : String {

    var returnVal = value
    if ( value != null || value != "" ) {
      returnVal = value.toInt() as String
    }

    return returnVal
  }

 /**
  * If the given code and code from the code reason string match then return true, otherwise return false.
  **/
  function mandatoryIfCode( code : String, codeReason : String ) : boolean {

    var codeReasonCode = codeReason.substring( 0, ( codeReason.indexOf( "-" ) - 1 ) ).trim()

    return codeReasonCode == code
  }


 /**
  * If the user has chosen to modify the RSR Update record mark it as a modification, otherwise (chosen to remove) reset the values
  * from the original RSR List record and mark it as a deletion.
  **/
  function actionModifyOrRemove( modifyOrRemove : String, rsrList : RSRList_Ext, rsrListUpdate : RSRListUpdate_Ext ) {

    if ( modifyOrRemove == "Modify" ) {
      rsrListUpdate.RegistrationCode = typekey.RSRRegistrationCode_Ext.TC_3
    } else if ( modifyOrRemove == "Remove" ) {
      rsrListUpdate = resetRSRListUpdateValues( rsrList, rsrListUpdate )
      rsrListUpdate.RegistrationCode = typekey.RSRRegistrationCode_Ext.TC_2
    }
  }

 /**
  * Return true if the RSR update record has either been marked as a deletion, or marked as a modification and has at
  * least one changed field. If neither condition is true or the record remains marked as a creation then return false.
  **/
  function isRemovalOrFieldsWereChanged( rsrListUpdate : RSRListUpdate_Ext, rsrList : RSRList_Ext ) : boolean {

    var returnVal = false

    if ( rsrListUpdate.RegistrationCode == typekey.RSRRegistrationCode_Ext.TC_1 ) {
      returnVal = false
    }

    else if ( rsrListUpdate.RegistrationCode == typekey.RSRRegistrationCode_Ext.TC_2 ) {
      returnVal = true
    }

    else if ( rsrListUpdate.RegistrationCode == typekey.RSRRegistrationCode_Ext.TC_3 ) {
      returnVal = haveFieldValuesChanged( rsrListUpdate, rsrList )
    }

    return returnVal
  }

 /**
  * Check to see whether any fields apart from the registration code have changed. If the RSR update record has been
  * marked as a modification but no meaningful changes have been made then it should not be possible to save the RSR
  * update record.
  **/
  function haveFieldValuesChanged( rsrListUpdate : RSRListUpdate_Ext, rsrList : RSRList_Ext ) : boolean {

    var returnVal = false
    var changeableFields = rsrListUpdate.ChangedFields
    changeableFields.removeWhere( \ field -> field == "RegistrationCode" || field == "PolicyNumber" )

    changeableFields.each( \ fld -> {
      var newValue = rsrListUpdate.getFieldValue( fld )
      newValue = ( newValue typeis String || newValue typeis java.util.Date ) ? newValue : newValue as String
   /* Unfortunately the following field is named differently in each entity, so we have to adjust */
      var fld2 = ( fld == "JuridicalForm" ) ? "JudicialForm" : fld
      if ( newValue != rsrList.getFieldValue( fld2 ) ) {
        if ( fld == "CodeReason" ) {
          var codeReasonTrimmed = ( rsrListUpdate.getFieldValue( fld ) as String ).substring( 0, 2 )
          var originalCodeReason = rsrList.getFieldValue( fld2 ) as String
          if ( codeReasonTrimmed != originalCodeReason ) {
            returnVal = true
          } else {
          }
        } else {
          returnVal = true
        }
      }
    } )

    return returnVal
  }

 /**
  * Before the current location is cancelled, remove the new rsrListUpdate object as it should not be persisted.
  **/
  function beforeCancel( rsrListUpdate : RSRListUpdate_Ext ) {

    rsrListUpdate.remove()
  }

 /**
  * Before the current location is committed, add an event to send an update to the RSR staging table.
  **/
  function beforeCommit( rsrListUpdate : RSRListUpdate_Ext ) {

    if ( _log.isTraceEnabled() ) {
      _log.trace( "beforeCommit: addEvent( ${RSRConstants.sendRSRUpdate} )" )
    }

    rsrListUpdate.addEvent( RSRConstants.sendRSRUpdate )
  }

 /**
  * Before the current location is validated, check whether we should throw a user displayable exception either because
  * it's not a removal (deletion) or it's a modification but only the registration code is different from the original.
  **/
  function beforeValidate( rsrListUpdate : RSRListUpdate_Ext, rsrList : RSRList_Ext ) {

    if ( not isRemovalOrFieldsWereChanged( rsrListUpdate, rsrList ) ) {

      throw new DisplayableException( displaykey.Web.Admin.RSR_Ext.Validation.NoFieldsChanged )
    }
  }
}