package gw.acc.rsr.admin
uses gw.acc.rsr.RSRConstants
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.search.EntitySearchCriteria
uses org.slf4j.Logger

uses java.util.Date
uses gw.api.database.DBFunction

/**
 * Created with IntelliJ IDEA.
 * User: vpennington
 */
@Export
class RSRListUpdate_ExtSearchCriteria extends EntitySearchCriteria< RSRListUpdate_Ext > {

  public static final var _log : Logger = RSRConstants._log

  var _sender   : User as SenderID
  var _dateFrom : Date as DateFrom
  var _dateTo   : Date as DateTo

 /**
  * Make the query and add in any restrictions based on what has been entered as search criteria.
  **/
  function makeQuery() : Query< RSRListUpdate_Ext > {

    var query = new Query< RSRListUpdate_Ext >( RSRListUpdate_Ext )

    if ( SenderID != null ) {
      query.compare( "CreateUser", Relop.Equals , SenderID )
    }

    if ( DateFrom != null ) {
      var trimmedDateFrom = DateFrom.trimToMidnight()
      query.compare( "CreateTime", Relop.GreaterThanOrEquals, DateFrom )
    }

    if ( DateTo != null ) {
      var trimmedDateTo = DateTo.addDays( 1 ).trimToMidnight()
      query.compare( "CreateTime", Relop.LessThanOrEquals, trimmedDateTo )
    }

    return query
  }

  override protected function doSearch() : IQueryBeanResult< RSRListUpdate_Ext > {

    return makeQuery().select()
  }

  override protected property get InvalidSearchCriteriaMessage() : String {

    return null
  }

  override protected property get MinimumSearchCriteriaMessage() : String {

    if ( _sender != null || ( _dateFrom != null && _dateTo != null ) ) {

      return null
    }

    return displaykey.Web.Admin.RSR_Ext.UpdateSearchPage.MiniumuSearchCriteria

  }

 /* This will check that the minimally acceptable information has been supplied
 * and perform the query otherwise it will throw an exception.
 */
  function validateAndSearch() : RSRListUpdate_ExtQuery {

    if ( this.MinimumCriteriaForSearch ) {

      return this.performSearch()
    }

    throw new gw.api.util.DisplayableException( displaykey.Web.Admin.RSR_Ext.RSRListUpdate_ExtQuery.NotEnoughInfo )

  }

/** This is the minimally acceptable information for our query.
 */
  property get MinimumCriteriaForSearch() : boolean {

    return ( this.SenderID != null || ( this._dateFrom != null && this._dateTo != null ) )
  }
}