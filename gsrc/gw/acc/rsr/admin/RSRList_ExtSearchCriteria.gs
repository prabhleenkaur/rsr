package gw.acc.rsr.admin

uses gw.acc.rsr.RSRConstants
uses gw.api.database.IQueryBeanResult
uses gw.api.database.Query
uses gw.api.database.Relop
uses gw.search.EntitySearchCriteria
uses org.slf4j.Logger

uses java.util.Date

/**
 * Created with IntelliJ IDEA.
 * User: vpennington
 */
@Export
class RSRList_ExtSearchCriteria extends EntitySearchCriteria< RSRList_Ext > {

  public static final var _log : Logger = RSRConstants._log

  var _firstName   : String as FirstName
  var _name        : String as Name
  var _vatNumber   : String as VATNumber
  var _dateOfBirth : Date   as DateOfBirth

 /**
  * Make the query and add in any restrictions based on what has been entered as search criteria.
  **/
  function makeQuery() : Query< RSRList_Ext > {

    var query = new Query< RSRList_Ext >( RSRList_Ext )
    addRSRDossierNumberRestriction( query )

    if ( FirstName != null ) {
      addFirstNameRestriction( query )
    }

    if ( Name != null ) {
      addNameRestriction( query )
    }

    if ( VATNumber != null ) {
      addVATNumberRestriction( query )
    }

    if ( DateOfBirth != null ) {
      addDateOfBirthRestriction( query )
    }

    return query
  }

 /**
  * Always add a check for a non-null RSR Dossier Number as a restriction to the query to ensure that we only return
  * records for this carrier.
  **/
  private function addRSRDossierNumberRestriction( query : Query< RSRList_Ext > ) {

    query.and( \ and0 -> {
      and0.compare( "RSRDossierNumber", Relop.NotEquals , null )
    } )
  }

 /**
  * If a first name criterion has been entered, then we'll make the expected alternative forms and add each as a
  * restriction to the query.
  **/
  private function addFirstNameRestriction( query : Query< RSRList_Ext > ) {

    query.and( \ and1 -> {
      and1.or( \ or1 -> {
        or1.compare( "FirstName", Relop.Equals, FirstName.toUpperCase() )
        or1.compare( "FirstName", Relop.Equals, FirstName.capitalize() )
        or1.compare( "FirstName", Relop.Equals, makeCustomName( FirstName.toUpperCase(), false ) )
        or1.compare( "FirstName", Relop.Equals, makeCustomName( FirstName.capitalize(), true ) )
      } )
    } )
  }

 /**
  * If a first name criterion has been entered, then we'll make the expected alternative forms and add each as a
  * restriction to the query.
  **/
  private function addNameRestriction( query : Query< RSRList_Ext > ) {

    query.and( \ and2 -> {
      and2.or( \ or2 -> {
        or2.compare( "Name", Relop.Equals, Name.toUpperCase() )
        or2.compare( "Name", Relop.Equals, Name.capitalize() )
      } )
    } )
  }

 /**
  * If a VAT number criterion has been entered, then we'll make the expected alternative forms and add each as a
  * restriction to the query. Old VAT Numbers are just a sequence of single digits, the new form is as the old form but
  * preceded by "BE0".
  **/
  private function addVATNumberRestriction( query : Query< RSRList_Ext > ) {

    var trimmedVATNumber = ( VATNumber.startsWithIgnoreCase( "BE0" ) ) ? VATNumber.remove( "BE0" ) : VATNumber
    var extendedVATNumber = ( VATNumber.startsWithIgnoreCase( "BE0" ) ) ? VATNumber : "BE0" + VATNumber
    query.and( \ and3 -> {
      and3.or( \ or2 -> {
        or2.compare( "VATNumber", Relop.Equals, trimmedVATNumber )
        or2.compare( "VATNumber", Relop.Equals, extendedVATNumber )
      } )
//      and3.compare( "VATNumber", Relop.Equals, trimmedVATNumber )
    } )
  }

 /**
  * If a date of birth criterion has been entered, then we'll add it as a restriction to the query.
  **/
  private function addDateOfBirthRestriction( query : Query< RSRList_Ext > ) {

    query.and( \ and4 -> {
      and4.compare( "DateOfBirth", Relop.Equals, DateOfBirth )
    } )
  }

 /**
  * This function makes an alternative form of the given name. It is assumed that names will either be all uppercase,
  * capitalized or uppercase with the second character lowercase or capitalized with the second character uppercase.
  * Starting with either a uppercase or a capitalized form, the alternative form with the adjusted second character is
  * returned. Where a name starts with two identical characters the first character is first stripped off before
  * processing and then re-added to form the full name.
  **/
  private function makeCustomName( formattedName : String, up : boolean ) : String {

    var firstChar = formattedName.charAt( 0 )
    var secondChar = formattedName.charAt( 1 )
    var firstCharStr = new String().valueOf( firstChar )
    var secondCharStr = new String().valueOf( secondChar )
    var formattedSecondCharStr = ( up ) ? secondCharStr.toUpperCase() : secondCharStr.toLowerCase()
    var beforeAndAfter : String[]
    var before : String
    if ( firstChar == secondChar ) {
      var nameLength = formattedName.length
      beforeAndAfter = formattedName.substring( 1, nameLength ).split( secondCharStr, 2 )
      before = firstCharStr + beforeAndAfter.first()
    } else {
      beforeAndAfter = formattedName.split( secondCharStr, 2 )
      before = beforeAndAfter.first()
    }
    var after = beforeAndAfter.last()
    var customName = before + formattedSecondCharStr + after

    return customName
  }

  override protected function doSearch() : IQueryBeanResult< RSRList_Ext > {
    return makeQuery().select()
  }

  override protected property get InvalidSearchCriteriaMessage() : String {

    return null
  }

  override protected property get MinimumSearchCriteriaMessage() : String {

    if ( _name == null ) {

      return displaykey.Web.Admin.RSR_Ext.ListSearchPage.MiniumuSearchCriteria

    }

    return null
  }

 /**
  * This will check that the minimally acceptable information has been supplied
  * and perform the query otherwise it will throw an exception.
  **/
  function validateAndSearch() : GroupQuery {

    if ( this.MinimumCriteriaForSearch ) {

      return this.performSearch()
    }

    throw new gw.api.util.DisplayableException( displaykey.Web.GroupSearch.NotEnoughInfo )
  }

 /**
  * This is the minimally acceptable information for a query
  **/
  property get MinimumCriteriaForSearch() : boolean {

    return this.Name != null
  }
}