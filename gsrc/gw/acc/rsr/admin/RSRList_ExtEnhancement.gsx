package gw.acc.rsr.admin

uses gw.api.database.Query
uses org.slf4j.Logger
uses gw.api.database.Relop
uses gw.acc.rsr.RSRConstants
/**
 * Created with IntelliJ IDEA.
 * User: vpennington
 * Date: 7/20/16
 * Time: 1:23 PM
 * To change this template use File | Settings | File Templates.
 */
enhancement RSRList_ExtEnhancement : entity.RSRList_Ext {

 /**
  * Using the RSR Number as the key query for the corresponding record in te Records_sent_Datassur_Ext table.
  * If corresponding records are found form a hashset based on the policy number field. If there is only one policy
  * number return that. if there is more then one policy number return an appropriate string. Log if there is more than
  * one matching policy number
  **/
  property get RelatedPolicyNo() : String {

    var _log = RSRConstants._log
    var rsrNumber = this.RSRNumber
    var query = Query.make( Records_sent_Datassur_Ext ).compare( "RSRNumber", Relop.Equals, rsrNumber )
    var results = query.select()
    var msgNotFound = displaykey.Web.Admin.RSR_Ext.Details.PolicyNumber.NotFound.Value

    if ( results.Count > 1 ) {
      var hSet = new java.util.HashSet< String >()
      results.each( \ r -> {
        hSet.add( r.PolicyNumber )
      } )
      if ( hSet.Count == 1 ) {

        return results.first().PolicyNumber
      } else {
        if (_log.isTraceEnabled() ) {
          _log.trace( "A unique policy number was not found for RSR Number: ${rsrNumber}")
        }

        return msgNotFound
      }
    } else if ( results.Count == 0 ) {

      return msgNotFound
    } else {

      return results.first().PolicyNumber
    }
  }

}
