package gw.acc.rsr.admin

uses java.util.HashMap
uses gw.api.database.Query

/**
 * Created with IntelliJ IDEA.
 * User: vpennington
 */
enhancement RSRValueCheck_ExtEnhancement : entity.RSRValueCheck_Ext {

  property get ReasonCodes() : HashMap< String, String > {

    var codeReasonsMap = new HashMap< String, String >()

    var query = Query.make( RSRValueCheck_Ext )
    var results = query.select()
    var checkValues = results.toTypedArray()
    var checkValuesByCode = checkValues.partition( \ cv -> cv.rsrCode )
    checkValuesByCode.eachKeyAndValue( \ code, reasons -> {
      var reasonStr : String = null
      var reasonCode : String = null
      if ( reasons.Count == 0 ) {
        print( "Something went wrong - no reasons found for this code ..." )
      }
      else if ( reasons.Count > 1 ) {
        reasons.each( \ re -> {
          var separator = ( reasonStr == null ) ? "" : " or "
          reasonStr = reasonStr + separator + typekey.RSRCancelReason_Ext.get( re.rsrCode ).DisplayName
        } )
      } else {
        reasonCode = reasons.single().rsrReason
        reasonStr = typekey.RSRCancelReason_Ext.get( reasonCode ).DisplayName
      }
    } )

    return codeReasonsMap
  }

}
