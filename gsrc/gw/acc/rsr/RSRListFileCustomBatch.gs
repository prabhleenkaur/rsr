package gw.acc.rsr

uses gw.processes.BatchProcessBase
uses java.lang.Exception
uses org.slf4j.Logger
uses org.slf4j.LoggerFactory
uses java.io.File
uses java.io.BufferedReader
uses java.io.FileReader
uses java.util.Date
uses java.text.SimpleDateFormat
uses util.IntegrationPropertiesUtil
uses org.apache.commons.io.FileUtils;
/**
 * Created with IntelliJ IDEA.
 * User:
 * Shikha Agarwal
 * Date: 13/06/16
 * Time: 3:01 PM
 * To change this template use File | Settings | File Templates.
 */
class RSRListFileCustomBatch extends BatchProcessBase {

  public static final var _log: Logger = RSRConstants._log
  static final var CLASS_NAME = "RSRListFileCustomBatch :::"

  /**
   * To construct the Batch Process
   */
  construct() {
    super(BatchProcessType.TC_RSRLISTFILE_EXT)
  }

  /**
   * Override Function to check the Initial Conditions
   */
  override function checkInitialConditions(): boolean {
    return true
  }

  /**
   * Do Work - To populate the policy center table,pcx_rsrlist_ext with records in RSRList.
   */
  override function doWork() {

    var METHOD_NAME = CLASS_NAME + "doWork"
    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodIN)
    }
    OperationsExpected = 0
    var targetPath = ""
    var folderName = ""
    var fileName = ""
    try {
      if (TerminateRequested) {
        throw new Exception("Terminate Requested!")
      }
      var file: File = null
      var target = IntegrationPropertiesUtil.getProperty("RSR_FOLDER_PATH")
      var f=new File(target)
      var allSubFiles=f.listFiles()
      var fileFound = "0"
      for ( f1 in allSubFiles)
      {
        fileName = f1.toString().substring(f1.toString().lastIndexOf("\\")+1,f1.toString().lastIndexOf("."))
        var currentDate  = dateToStringConversion(gw.api.util.DateUtil.currentDate())
        var ddMM = currentDate.substring(0,2) + currentDate.substring(2,4)
        if((fileName.length  == RSRConstants.RSR_LIST_FILE_LENGTH) and (fileName.startsWith(RSRConstants.RSR_LIST_FILE_RSR)) and (fileName.substring(8).equals(ddMM)) )
        {
          folderName = IntegrationPropertiesUtil.getProperty("RSR_FOLDER_PATH")
          targetPath = folderName+fileName+RSRConstants.REPORT_FILE_EXT
          fileFound = "1"
          break
        }
      }
      switch(fileFound){
        case "1" :
            _log.debug("RSRListFileCmustomBatch: targetPath : " + targetPath)
            file = new File(targetPath)
            var status = updateDatabase(file,targetPath,fileName)
            break
        case "0" :
            break
          default :
          break
      }
      if(_log. isDebugEnabled()) {
        _log.trace(METHOD_NAME + " ****OperationsExpected****" + OperationsExpected)
      }
    } catch (ex: Exception) {
      incrementOperationsFailed()
      OperationsFailedReasons.add(ex.Message)
      _log.error("Exception occured", ex +":::" +ex.StackTraceAsString)
    } finally {
      incrementOperationsCompleted()
    }

    if(_log. isTraceEnabled()) {
      _log.trace(METHOD_NAME + RSRConstants._methodOUT)
    }
  }

  /**
   * Support termination before each query export in the iteration
   */
  override function requestTermination(): boolean {
    return true
  }

  /**
   * Convert String to Date
   */
  @Param("dateString", String)
  @Returns("Date in a specific format")
  private  function stringToDateConversion(dateString: String): Date {
    var dateFormatter = new SimpleDateFormat("ddMMyyyy")
    if ((dateString != null) and (dateString !="        ")) {
      return (dateFormatter.parse(dateString) )
    }
    else
      return null
  }

  /**
   * Convert Date to String
   */
  @Param("date", Date)
  @Returns("String")
  private  function dateToStringConversion(date: Date): String {
    var formatter = new SimpleDateFormat("ddMMyyyy");
    var str = formatter.format(date)
    return   str
  }

  /**
   * Update database if RSR List file is found
   */
  @Param("file", File)
  @Param("targetPath", String)
  @Param("fileName", String)
  @Returns("flag depending on success or failure")
  private function updateDatabase(file: File,targetPath: String,fileName: String): int {
    if (file.canRead()) {
      _log.debug(" RSRListFileCustomBatch  file can read..")
    }
    var br: BufferedReader = null
    var errorStatus: int = 0

    try{
      br = new BufferedReader(new FileReader(file))
      var line = br.readLine()
      var  firstName = ""
      var  dateOfBirth = ""
      var  gender = ""
      var juridicalForm = ""
      var  vatNumber = ""
      var   numadd  =  0
      var   numdel  =  0
      var   nummod  =  0
      while (line != null && line != "") {
        if (line.NotBlank)   {
          var name = line.toString().substring(0,35)
          var street = line.toString().substring(35,65)
          var houseNumber = line.toString().substring(65,70)
          var boxNumber = line.toString().substring(70,74)
          var zipCode = line.toString().substring(74,81)
          var city = line.toString().substring(81,105)
          var langCode = line.toString().substring(105,106)
          var companyPerson = line.toString().substring(106,107)
          switch (companyPerson)
          {
            case "1" :

                firstName = line.toString().substring(107,122)
                dateOfBirth = line.toString().substring(122,130)
                gender = line.toString().substring(130,131)
                break;
            case "2" :

                juridicalForm = line.toString().substring(107,109)
                vatNumber = line.toString().substring(109,118)
                break;
          }
          var riskStreet = line.toString().substring(142,172)
          var riskHouseNumber = line.toString().substring(172,177)
          var riskBoxNumber = line.toString().substring(177,181)
          var riskZipCode = line.toString().substring(181,188)
          var riskCity = line.toString().substring(188,212)
          var role = line.toString().substring(212,216)
          var insBranch = line.toString().substring(216,226)
          var eventDate = line.toString().substring(263,271)
          var identificationDate = line.toString().substring(271,279)
          var reasonCode = line.toString().substring(279,289)
          var registrationCode = line.toString().substring(290,291)
          var dispute = line.toString().substring(291,292)
          var doubtableAddress = line.toString().substring(292,293)
          var numberClaimsInvolved = line.toString().substring(293,295)
          var numberClaimsFault = line.toString().substring(295,297)
          var amount = line.toString().substring(297,306)
          var releaseCode = line.toString().substring(315,316)
          var ficheRSR = line.toString().substring(323,328)
          var numberRSR = line.toString().substring(328,335)
          var dossierRSRMod = line.toString().substring(335,342)
          var RSRSheet =  line.toString().substring(342,350)
          gw.transaction.Transaction.runWithNewBundle(\bundle -> {
            switch (registrationCode)
            {
              case "1" :
                  numadd++
                  var rsrListRecord = new RSRList_Ext()    //in case of addition
                  bundle.add(rsrListRecord)
                  rsrListRecord.Name   = name
                  rsrListRecord.Street   = street
                  rsrListRecord.HouseNumber   = houseNumber
                  rsrListRecord.BoxNumber   = boxNumber
                  rsrListRecord.ZipCode   = zipCode
                  rsrListRecord.City   = city
                  rsrListRecord.LanguageCode   = langCode
                  rsrListRecord.PersonCompany   = companyPerson
                  switch (companyPerson)
                  {
                    case "1" :

                        rsrListRecord.FirstName   = firstName
                        rsrListRecord.DateOfBirth   = stringToDateConversion(dateOfBirth)
                        rsrListRecord.Gender   = gender
                        break
                    case "2" :

                        rsrListRecord.Judicialform   = juridicalForm
                        rsrListRecord.VATNumber   = vatNumber
                        break;
                  }
                  rsrListRecord.RiskStreet = riskStreet
                  rsrListRecord.RiskHouseNumber = riskHouseNumber
                  rsrListRecord.RiskBoxNumber = riskBoxNumber
                  rsrListRecord.RiskZipCode = riskZipCode
                  rsrListRecord.RiskCity = riskCity

                  rsrListRecord.Role = role
                  rsrListRecord.InsuranceBranch = insBranch
                  rsrListRecord.EventDate   = stringToDateConversion(eventDate)
                  rsrListRecord.EventIdentificationDate   = stringToDateConversion(identificationDate)
                  rsrListRecord.CodeReason = reasonCode
                  rsrListRecord.RegistrationCode = registrationCode
                  rsrListRecord.Dispute = dispute
                  rsrListRecord.DoubtableAddress = doubtableAddress
                  rsrListRecord.NumberClaims_RegisteredPerson = numberClaimsInvolved
                  rsrListRecord.NumberClaims_RegisteredFault = numberClaimsFault
                  rsrListRecord.Amount = amount
                  rsrListRecord.ReleaseCode = releaseCode
                  rsrListRecord.FicheRSR = ficheRSR
                  rsrListRecord.RSRNumber = numberRSR
                  rsrListRecord.RSRDossierNumber =  dossierRSRMod
                  rsrListRecord.RSRSheetDate = stringToDateConversion(RSRSheet)
                  break;
              case "2"  :
                  numdel++
                  var rsrList_ExtQuery = gw.api.database.Query.make(RSRList_Ext)      //in case of deletion
                  rsrList_ExtQuery.compare(RSRList_Ext#RSRDossierNumber,Equals,dossierRSRMod)
                  var recordToBeDeleted  = rsrList_ExtQuery.select().first()
                  bundle.add(recordToBeDeleted)
                  bundle.delete(recordToBeDeleted)
                  break;
              case "3"  :
                  nummod++
                  var rsrList_ExtQuery = gw.api.database.Query.make(RSRList_Ext)      //in case of modification
                  rsrList_ExtQuery.compare(RSRList_Ext#RSRDossierNumber,Equals,dossierRSRMod)
                  var recordToBeModified  = rsrList_ExtQuery.select().first()
                  bundle.add(recordToBeModified)
                  bundle.delete(recordToBeModified)
                  recordToBeModified = new RSRList_Ext()
                  bundle.add(recordToBeModified)
                  recordToBeModified.Name   = name
                  recordToBeModified.Street   = street
                  recordToBeModified.HouseNumber   = houseNumber
                  recordToBeModified.BoxNumber   = boxNumber
                  recordToBeModified.ZipCode   = zipCode
                  recordToBeModified.City   = city
                  recordToBeModified.LanguageCode   = langCode
                  recordToBeModified.PersonCompany   = companyPerson
                  switch (companyPerson)
                  {
                    case "1" :
                        recordToBeModified.FirstName   = firstName
                        recordToBeModified.DateOfBirth   = stringToDateConversion(dateOfBirth)
                        recordToBeModified.Gender   = gender
                        break
                    case "2" :
                        recordToBeModified.Judicialform   = juridicalForm
                        recordToBeModified.VATNumber   = vatNumber
                        break;
                  }
                  recordToBeModified.RiskStreet = riskStreet
                  recordToBeModified.RiskHouseNumber = riskHouseNumber
                  recordToBeModified.RiskBoxNumber = riskBoxNumber
                  recordToBeModified.RiskZipCode = riskZipCode
                  recordToBeModified.RiskCity = riskCity
                  recordToBeModified.Role = role
                  recordToBeModified.InsuranceBranch = insBranch
                  recordToBeModified.EventDate   = stringToDateConversion(eventDate)
                  recordToBeModified.EventIdentificationDate   = stringToDateConversion(identificationDate)
                  recordToBeModified.CodeReason = reasonCode
                  recordToBeModified.RegistrationCode = registrationCode
                  recordToBeModified.Dispute = dispute
                  recordToBeModified.DoubtableAddress = doubtableAddress
                  recordToBeModified.NumberClaims_RegisteredPerson = numberClaimsInvolved
                  recordToBeModified.NumberClaims_RegisteredFault = numberClaimsFault
                  recordToBeModified.Amount = amount
                  recordToBeModified.ReleaseCode = releaseCode
                  recordToBeModified.FicheRSR = ficheRSR
                  recordToBeModified.RSRNumber = numberRSR
                  recordToBeModified.RSRDossierNumber =  dossierRSRMod
                  recordToBeModified.RSRSheetDate = stringToDateConversion(RSRSheet)
                  break;
                default :
                break;
            }
          }, User.util.CurrentUser.Credential.UserName)
        }
        line = br?.readLine()
      }
      var RSRListQuery = gw.api.database.Query.make(RSRList_Ext)
      var result = RSRListQuery.select().Count
      _log.debug("Number of Records in table RSRList_Ext"+result)
      _log.debug("Number of Records Added"+numadd)
      _log.debug("Number of Records Deleted"+numdel)
      _log.debug("Number of Records Modified"+nummod)
    }   catch (e: Exception) {
      _log.error("Error while parsing and saving the flat file data into PolicyCeinter Table : " + "\\n" + e.StackTraceAsString)
      errorStatus = - 1
    }
        finally {
      if (br != null) {
        br.close()
        file = new File(targetPath)
        var folderName = IntegrationPropertiesUtil.getProperty("RSR_FOLDER_PATH")
        targetPath = folderName + fileName + "_processed" + RSRConstants.REPORT_FILE_EXT
        var success = file.renameTo(new File(targetPath))
        _log.debug("file has been processed and renamed to "+fileName+"_processed"+RSRConstants.REPORT_FILE_EXT)
      }
    }
    return errorStatus
  }
}

