package gw.acc.rsr

uses java.util.Date
uses gw.web.policy.PolicyBillingUIHelper
uses gw.plugin.Plugins

/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 12/11/15
 * Time: 3:13 PM
 * Policy Period Enhancement for RSR
 */
enhancement PolicyPeriod_RSREnhancement:entity.PolicyPeriod {
  /**
   * Property to get the PolicyPeriod Payment Due Date
   */
  property get paymentDueDate(): Date {
    var billingPlugin = Plugins.get(gw.plugin.billing.IBillingSummaryPlugin)
    var sortedTermNum = PolicyBillingUIHelper.getSortedTermNumbers(this.Policy)
    var termNumIndex = PolicyBillingUIHelper.getTermNumberIndex(this, sortedTermNum)
    var billingSummary = PolicyBillingUIHelper.retrieveBillingSummary(billingPlugin, this.PolicyNumber, sortedTermNum.Count)

    return billingSummary.Invoices.orderByDescending(\i -> i.InvoiceDueDate).first().InvoiceDueDate
  }

  /**
   * Function to validate the RSR Reason - 16 (16 is a fixed value of RSR Reason :"Insurer refuses to pay the compensation to the insured due to contractual reasons or condemnable behavior "
   */
  function checkReason_16(cancellation: Cancellation): Boolean {
    var reasonCodeName = cancellation.CancelReasonCode
    var reasonCodeCancel = this.Cancellation.CancelReasonCode
    var retRSR16: boolean
    if (reasonCodeCancel != null) {
      retRSR16 = (cancellation != null or (typekey.ReasonCode.TF_RSRREASON16.TypeKeys.contains(reasonCodeCancel)))
    }
    else if (reasonCodeName != null){
      retRSR16 = (this.Job typeis Cancellation and this.CancellationDate != null or (typekey.ReasonCode.TF_RSRREASON16.TypeKeys.contains(reasonCodeName)))
    }
    return retRSR16
  }

  /**
   * Function to validate the RSR Reason - 17 (17 is a fixed value of RSR Reason :"Insurer pays the compensation but claims a refund from the insured"
   */
  function checkReason_17(cancellation: Cancellation): Boolean {
    var retRSR17 = (this.Job typeis Cancellation and this.CancellationDate != null and
        ((this.Cancellation.CancelReasonCode == ReasonCode.TC_UWREASONS) and
            this.Cancellation.RSRData_ExtID.rsrCancelReason == RSRCancelReason_Ext.TC_CLAIMSREFUND)) or
        (cancellation != null and (cancellation.CancelReasonCode == ReasonCode.TC_UWREASONS))

    return retRSR17
  }

  /**
   * Function to validate the RSR Reason - 31 (31 is a fixed value of RSR Reason :"Prospect provides information contrary to historical information in the RSR database "provides information contrary to historical information in the RSR database")
   */
  function checkReason_31(submission: Submission): Boolean {
    var retRSR31 = ((this.Job typeis Submission ) and (this.Submission.RejectReason == ReasonCode.TC_PRODREQUIREMENTS or
        this.Submission.RejectReason == ReasonCode.TC_INFONOTPROVIDED ))  or
        (submission != null and (submission.RejectReason == ReasonCode.TC_PRODREQUIREMENTS or submission.RejectReason == ReasonCode.TC_INFONOTPROVIDED))
    return retRSR31
  }

  /**
   * Function to concatenate the PolicyContacts Public ID
   */
  function getPolicyContactsID(): String {
    var policyContactIds: String = null
    var policyCons = this.AccountContactRoleMap
    var rsrCons = policyCons.keySet().where(\elt -> policyCons.get(elt).where(\elt1 -> (elt1 typeis PolicyNamedInsured) or (elt1 typeis PolicyAddlInsured) or (elt1 typeis PolicyDriver)).Count > 0)
    for (con in rsrCons) {
      // Above line adds all those Account contacts in rsrCons which have policy contact role in (PolicyNamedInsured,PolicyAddlInsured,PolicyDriver) and the number of contact roles>0
      if (policyContactIds == null) {
        policyContactIds = con.Contact.PublicID
      } else {
        policyContactIds = policyContactIds + RSRConstants.CON_Separator + con.Contact.PublicID
      }
    }

    return policyContactIds
  }

  /**
   * Function to get the policy Contacts
   */
  function getPolicyContactsRSR(): List<AccountContact> {
    var contact_rsr = new List<AccountContact>()
    var policyCons = this.AccountContactRoleMap
    var rsrCons = policyCons.keySet().where(\elt -> policyCons.get(elt).where(\elt1 -> (elt1 typeis PolicyNamedInsured) or (elt1 typeis PolicyAddlInsured) or (elt1 typeis PolicyDriver)).Count > 0)
    return rsrCons
  }

  /*
  Function to return all contacts related to a policy
   */

  function getAllAccountContacts(): List<AccountContact> {
    var contact_rsr = new List<AccountContact>()
    var policyCons = this.AccountContactRoleMap
    var rsrCons = policyCons.keySet()
    for (con in rsrCons) {
      contact_rsr.add(con)
    }
    return contact_rsr.Count > 0 ? contact_rsr : {}
  }

  /**
   * Function to get the policy Drivers for Commercial Auto Only
   */
  function getPolicyComDriversRSR(): List<CommercialDriver> {
    var contact_rsr = new List<CommercialDriver>()
    var businessLine = this.Lines.firstWhere(\elt -> elt typeis BusinessAutoLine)
    contact_rsr.addAll((businessLine as BusinessAutoLine).Drivers)
    return contact_rsr
  }

  /**
   * Function to validate the Policy Change for RSR
   */
  function validateRSR_policyChange(policyChange: PolicyChange): String {
    if (policyChange.GenerateRSR_Ext == true and policyChange.RSRPolicydata_ExtID == null) {
      return "Please click on the RSR Button to fill in the mandate Details. "
    }
    return null
  }

  /**
   * Function to get the policy Contacts
   */
  function getPolicyContacts_RSR(conIds: String): List<AccountContact> {
    var contact_rsr = new List<AccountContact>()
    var policyCons = this.AccountContactRoleMap
    conIds?.split(RSRConstants.CON_Separator)?.each(\conId -> {
      var rsrCon = policyCons.keySet().firstWhere(\elt -> elt.Contact.PublicID == conId)
      contact_rsr.add(rsrCon)
    })
    return contact_rsr.Count > 0 ? contact_rsr : {}
  }

  /**
   * Function to get the policy Drivers for Commercial Auto Only
   */
  function getPolicyComDrivers_RSR(conIds: String): List<CommercialDriver> {
    var contact_rsr = new List<CommercialDriver>()
    conIds?.split(RSRConstants.CON_Separator)?.each(\conId -> {
      if (conId != null) {
        contact_rsr.add(CommercialDriver(conId))
      }
    })

    return contact_rsr.Count > 0 ? contact_rsr : {}
  }

  function checkRSRVailability(): Boolean {
    var retVal = this.Policy.Jobs.hasMatch(\elt1 -> (elt1 typeis PolicyChange) and elt1.GenerateRSR_Ext == true and elt1.RSRPolicydata_ExtID != null)
    return retVal
  }
}
