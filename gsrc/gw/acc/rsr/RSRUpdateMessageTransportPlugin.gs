package gw.acc.rsr

uses gw.plugin.messaging.MessageTransport
uses org.slf4j.Logger
uses gw.acc.rsr.rsrdb.RSRDBBase

/**
 */
class RSRUpdateMessageTransportPlugin implements MessageTransport {

  public static final var DESTINATION_ID : int = 71
  public static final var _log : Logger = RSRConstants._log

  override function send( message : Message, payload : String ) {

    var METHOD_NAME = "RSRUpdateMessageTransportPlugin ::: send"
    var messageRoot = message.MessageRoot as RSRListUpdate_Ext
    print( METHOD_NAME )

    if ( _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + " :: messageRoot :: ${messageRoot.RSRDossierNumber} -- ${messageRoot.PublicID} " )
    }

    switch ( message.EventName ) {

      case RSRConstants.sendRSRUpdate :
        var root = message.MessageRoot as RSRListUpdate_Ext
        var xmlPayload = gw.acc.rsr.data.rsrlistupdate_extmodel.RSRListUpdate_Ext.parse( payload )
        if ( _log. isDebugEnabled() ) {
          _log.debug( METHOD_NAME + " -- payload -- " +xmlPayload.asUTFString() )
        }
        var integrationDB = new RSRDBBase()
        integrationDB.insertUpdateToRSRStaging( messageRoot )
        break

    }

    if (  _log. isTraceEnabled() ) {
      _log.trace( METHOD_NAME + RSRConstants._methodOUT )
    }
    message.reportAck()
  }

  override function shutdown() {
  }

  override function suspend() {
  }

  override function resume() {
  }

  override function setDestinationID(p0: int) {
  }

}