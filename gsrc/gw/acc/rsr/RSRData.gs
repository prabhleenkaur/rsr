package gw.acc.rsr

uses java.lang.StringBuilder
uses java.util.HashMap
uses java.util.Map
uses java.text.SimpleDateFormat
uses java.util.Date
uses java.io.File
uses util.IntegrationPropertiesUtil
/**
 * Created with IntelliJ IDEA.
 * User: Aishwarya.Lakshmi
 * Date: 30/10/15
 * Time: 1:52 PM
 * To change this template use File | Settings | File Templates.
 */
class RSRData {

  private final var _values = new HashMap<RSRField, String>()

  public static final enum RSRField {

    PolicyHolderName("name",35, 1),
    PolicyHolderStreet("PolicyHolder - Street", 30 ,36),
    PolicyHolderHouseNum("PolicyHolder - House Number",5 , 66),
    PolicyHolderBoxNum("PolicyHolder - Box Number",4, 71),
    PolicyHolderZipCode("PolicyHolder - ZIPCode", 7, 75),
    PolicyHolderCity("PolicyHolder - City", 24, 82),
    LangCode("PolicyHolder - Language Code", 1, 106),
    EntityType("Policy Holder - Individual / Legal Entity", 1, 107),
    FirstName("Policy Holder 's First Name if the Entity Type is Indivudual", 15, 108),
    DOB("Policy Holder 's DOB if the Entity Type is Indivudual", 8, 123),
    Gender("Policy Holder 's Gender if the Entity Type is Indivudual", 1, 131),
    JuridicalForm("Policy Holder's Org Type if the Entity Type is Company", 2, 108),
    VATNum("Policy Holder's Vat Num if the Entity Type is Company", 9, 110),
    RiskLocStreet("Risk Location Street in case of Fire Insurance Branch", 30, 143),
    RiskLocHouseNum("Risk Location House Number in case of Fire Insurance Branch", 5, 173),
    RiskLocBoxNum("Risk Location Box NUmber in case of Fire Insurance Branch", 4, 178),
    RiskLocZip("Risk Location ZipCode in case of Fire Insurance Branch", 7, 182),
    RiskLocCity("Risk Location City in case of Fire Insurance Branch", 24, 189),
    PersonRole("Role of the Person", 4, 213) ,
    InsAndSubInsBranch("List of InsBranches and Sub Branches related to the Policy", 10, 217),
    FSMACode("FSMA Code", 5,227),
    RSRPolicyNumber("PolicyNumber of the PolicyPeriod", 12, 232),
    SenderInfo("UserId of the RSR Report Creator", 20, 244),
    EventDate("Date of the event reason to be in RSR", 8, 264),
    RegistrationDate("Date of the RSR Registration", 8, 272),
    ReasonCode("RSR Reason Code", 10, 280),
    AddSpace("RSR Additional space",1,290),
    RegistrationCode("Code of Registration - whether creation/modification/deletion", 1, 291), //starting at 290
    Disputed("Is the PolicyHolder Disputing the event", 1, 292),
    DoubtableAddr("Is the PolicyHolder's address is doubtable", 1, 293),
    NoOfClaims("Number of claims with registered person involved", 2, 294),
    NoOfClaimsInfault("Number of claims with registered person In Fault", 2, 296),
    Amount("Amount", 9, 298),
    AddMoreSpace("RSR Add More space",9,307),
    Version("RSR Version", 1, 316),
    AddMoreSpaces("RSR Add More Spaces",12,317),
    RSRNumber("RSR Number (optional)", 7, 329),
    RSRDossierNumber("PolicyNumber - RSR dossier number (in case of deletion or modification)", 7, 336),
    AddEightMoreSpace("RSR Add Eight More space",8,343),

    var _name: String
    var _len: int
    var _index: int
    private construct(name: String, len: int, index: int) {
      _name = name
      _len = len
      _index = index
    }
  }

  /**
   * To construct the Hash Map
   */
  construct(map: Map<RSRField, String>) {
    _values.putAll(map)
  }


  /**
   * To get the HashMap RSR Data
   */
  public function getRSRData() : HashMap<RSRField, String> {
    return _values
  }


  /**
   * To write the RSR Entry in one row for each element
   */
  public function toRow(): String {
    var str = new StringBuilder(5700)
    var flag = 0
    for (f in RSRField.AllValues) {

      if(f._name.toString().equals("Policy Holder - Individual / Legal Entity") && (_values.get(f).toString().equals("2")))
      {
        flag=1
      }
      var v = _values.get(f)
      if (v == null) {
        if(((flag==1) && (f._name.toString().equals("Policy Holder 's First Name if the Entity Type is Indivudual")))
            || ((flag==1) && (f._name.toString().equals("Policy Holder 's DOB if the Entity Type is Indivudual")))
            || ((flag==1) && (f._name.toString().equals("Policy Holder 's Gender if the Entity Type is Indivudual")))
        )
        {}
        else
        {
          str.append(RSRPolicyHelper.getSpaces(f._len))
        }
      }
      else {
        str.append(RSRPolicyHelper.getFixedWitdhUTF8Value(v, f._len))
      }
      if((flag==1) && (f._name.toString().equals("Policy Holder's Vat Num if the Entity Type is Company")))
      {
        str.append(RSRPolicyHelper.getSpaces(24))
      }
    }

    return str.toString()
  }

  /**
   * To convert the Date to specific format
   */
  public static function convertDate(date : java.util.Date) : String {
    var dateFormat : SimpleDateFormat = new SimpleDateFormat("yyyyMMdd")
    var retDate = dateFormat.format(date)
    return  retDate
  }

  /**
   * To convert the Date to specific format
   */
  public static function convertDateEuroFormat(date : java.util.Date) : String {
    var dateFormat : SimpleDateFormat = new SimpleDateFormat("ddMMyyyy")
    var retDate = dateFormat.format(date)
    return  retDate
  }

  /**
   * Function to get the File Path
   */
  @Returns ("The path of the generated File")
  public static property get FilePath() : String {
    var path = new StringBuilder(IntegrationPropertiesUtil.getProperty("RSRExportPath")).append(File.separator)
    path.append(RSRConstants.REPORT_FILE_NAME)
        .append(new SimpleDateFormat(RSRConstants.REPORT_FILE_DATE_FORMAT).format(new Date()))
        .append(RSRConstants.REPORT_FILE_EXT);
    return path.toString()

  }


}